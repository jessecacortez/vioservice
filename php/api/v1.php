<?php

use Bad_Http as Http,
    Bad_Http_Request as Request,
    Bad_Http_Response as Response,
    vio\models\Violation,
    vio\services\Violation as Violations;

$app = require_once __DIR__ . "/../../misc/bootstrap.php";

/** Request -> bool */
function allowed(Request $req) {
    // TODO: check a magic request header
    return true;
}

/** Request -> bool */
function canPost(Request $req) {
    return true;
}

/** Response, str, str? -> () */
function ok(Response $res, $data, $contentType='application/json') {
    $res->setStatus(Http::OK)
        ->setHeader(Http::CONTENT_TYPE, $contentType)
        ->write($data);
}

/** Violation -> str{int code, name, year, int section} */
function extractBaseFrom(Violation $data) {
    return [
        'code' => $data->code,
        'name' => $data->name,
        'year' => $data->year,
        'section' => $data->section,
    ];
}

/** Violation[] -> str{int code, name, year, int section, 
 *                     str{datetime, description, security}[]}
 */
function groupViolations($data) {
    $violations = [];
    foreach ($data as $vio) {
        $violations[] = [
            'datetime' => $vio->datetime,
            'description' => $vio->description,
            'security' => $vio->security,
        ];
    }
    if (empty($vio)) {
        $result = [];
    } else {
        $result = extractBaseFrom($vio);
        $result['violations'] = $violations;
    }
    return $result;
}

/**
 * {str{description, datetime}[] violations}, int -> int[][]
 * assumes :violations is sorted by :description
 * TODO: should be in service class
 */
function indexUniques($data, $grpLimit=Violations::ALLOWED_DUPES) {
    $vios = $data['violations'];
    $last = null;
    $indices = [];
    $c = 0;
    foreach ($vios as $i => $vio) {
        $indices[] = $i;
        ++$c;
        if (empty($last)) {
            $last = $vio;
        }
        if ($last['description'] != $vio['description']
            || !sameDay($last['datetime'], $vio['datetime'])
            || $c % $grpLimit == 0
        ) {
            $uniques[] = $indices;
            $indices = [];
            $last = $vio;
            $c = 0;
        } 
        // it's actually possible to know if dataset is out of order while i'm
        // traversing it. but i won't.
    }
    if (!empty($indices)) {
        $uniques[] = $indices;
    }
    return $uniques;
}

/**
 * str, str -> bool
 * for simplicity, assumes dates of the format Y-m-d H:i:s
 */
function sameDay($date1, $date2) {
    $part1 = substr($date1, 0, strpos($date1, ' '));
    $part2 = substr($date2, 0, strpos($date2, ' '));
    return $part1 == $part2;
}

try {
    $http = $app->router;

    switch (true) {

    case $http->GET("/debug"):
        $svc = $app->model->violations;
        $raw = "code=21&name=abc&year=1st&section=7&datetime=2014-02-23+23:10:33&description=vio2&security=max";
        parse_str($raw, $data);
        $vio = Violation::parse($data);
        var_Dump($vio);
//        $svc->put($vio);
        die;
        break;

    case (!allowed($app->request)):
        $app->response
            ->setStatus(Http::FORBIDDEN)
            ->setHeader(Http::CONTENT_TYPE, "text/plain")
            ->write("Access denied.");
        break;

    // get EVERYTHING. make sure this doesn't happen too often.
    case $http->GET("/all"):
        $svc = $app->model->violations;
        $db = $app->model->PDO;
        $sql = "select * from data";
        $res = $db->prepare($sql);
        $res->execute();
        ok($app->response, json_encode(iterator_to_array($res)));
        break;

    // return timestamp of the latest row for syncing purposes
    case $http->GET('/last-entry'):
        $service = $app->model->violations;
        ok($app->response, json_encode($service->getLastEntry()));
        break;

    // sync data since last row entered by :security at :date and :time
    case $http->GET('/sync-from/:date(\d{4}-\d\d-\d\d)/:time(\d\d:\d\d:\d\d)/:security'):
        $service = $app->model->violations;
        $result = [];
        $args = $http->args();
        $security = $args->security;
        $date = "{$args->date} {$args->time}";
        $result = $service->getAllSinceAndBySecurity($date, $security);
        ok($app->response, json_encode(iterator_to_array($result)));
        break;

    // get all student violations by id
    case $http->GET('/student/:id(\d+)'):
        $service = $app->model->violations;
        $id = $http->args()->id;
        $result = groupViolations($service->getAllByCode($id));
        $result['unique_violations'] = indexUniques($result);
        ok($app->response, json_encode($result));
        break;

    // get all student violations by name
    case $http->GET('/student/:name'):
        $service = $app->model->violations;
        $name = $http->args()->name;
        $result = groupViolations($service->getAllByName($name));
        ok($app->response, json_encode($result));
        break;

    // get all violations by code, combine duplicates
    case $http->GET('/distinct/:code(\d+)'):
        $service = $app->model->violations;
        $code = $http->args()->code;
        $rows = $service->getByCodeGroupSameDayDuplicates($code, 3);
        $result = [];
        foreach ($rows as $row) {
            $result[] = groupViolations(new ArrayIterator($row));
        }
        ok($app->response, json_encode($result));
        break;

    // get all violations by name, combine duplicates
    case $http->GET('/distinct/:name'):
        $service = $app->model->violations;
        $name = urldecode($http->args()->name);
        $rows = $service->getByNameGroupSameDayDuplicates($name, 3);
        $result = [];
        foreach ($rows as $row) {
            $result[] = groupViolations(new ArrayIterator($row));
        }
        ok($app->response, json_encode($result));
        break;

    // get all violations on the month of :month
    case $http->GET('/monthly/:month(\d\d)'):
        break;

    // get all violations during :month grouped by :name
    case $http->GET('/monthly/:month(\d\d)/by-name/:name'):
        break;

    // get all violations during :month grouped by :code
    case $http->GET('/monthly/:month(\d\d)/by-code/:code(\d+)'):
        break;

    // gate for POST requests. put all $http->POST() conds below this.
    case ($app->request->isPost() && !canPost($app->request)):
        $app->response
            ->setStatus(Http::FORBIDDEN)
            ->setHeader(Http::CONTENT_TYPE, 'text/plain')
            ->write("Access denied.");
        break;

    // put new violation
    case $http->POST('/one'):
        $data = $app->request->body()->flatten();
        $service = $app->model->violations;
        $service->put(Violation::parse($data));
        ok($app->response, "1", "text/plain");
        break;

    // put lots of new violations
    case $http->POST('/bulk'):
        $data = $app->request->body()->violations;
        if (!empty($data) && is_array($data)) {
            $service = $app->model->violations;
            $service->putMany($service->toEntityCollection($data));
            ok($app->response, (string) count($data), "text/plain");
        } else {
            $app->response
                ->setStatus(Http::BAD_REQUEST)
                ->setHeader(Http::CONTENT_TYPE, 'text/plain')
                ->write("Malformed request. Try harder next time.");
        }
        break;

    // bad url
    default:
        $app->response
            ->setStatus(Http::NOT_FOUND)
            ->setHeader(Http::CONTENT_TYPE, 'text/plain')
            ->write("Resource not found.");
        break;
    }

} catch (Exception $e) {
    // TODO: log error
    $app->response
        ->setStatus(Http::INTERNAL_SERVER_ERROR)
        ->setHeader(Http::CONTENT_TYPE, 'text/plain')
        ->write("API call failed. Sorry. Exn msg: {$e->getMessage()}");
}

$app->response->send();
