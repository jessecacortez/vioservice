<?php

use Bad_Http as Http,
    Bad_Application as App,
    Bad_Http_Request as Request,
    Bad_Http_Response as Response,
    vio\support\view\Template,
    vio\support\CallbackMaxHeap as SortBy;

$app = require_once __DIR__ . '/../misc/bootstrap.php';

/** Response, str, str? -> () */
function ok(Response $response, $out, $type="text/html") {
    $response->setStatus(Http::OK)
        ->setHeader(Http::CONTENT_TYPE, $type)
        ->write($out);
}

/** Response, str -> () */
function redirect(Response $response, $to) {
    $response->setStatus(Http::SEE_OTHER)
        ->setHeader(Http::LOCATION, $to);
}

/** Application, str, any{} -> str */
function render(App $app, $tpl, $vars) {
    $defs = ["url" => $app->url];
    return $app->view->renderer->render(new Template($tpl), $defs + $vars);
}

$res = $app->response;
$http = $app->router;
$url = $app->url;
try {
    switch (true) {

    case $http->GET('/'): // 2 intentional cascades
    case $http->GET('/this-week'):
    case $http->match('/', 'GET'): // for when index.php is absent
        $tz = new DateTimeZone("Asia/Manila");
        $date = new DateTime("last Monday", $tz);
        $now = new DateTime("now", $tz);
        $start = $date->format("Y-m-d");
        $end = $now->format("Y-m-d");
        $week = $app->model->violations->getAllInDateRange($start, $end);
        $vars = [
            'data' => SortBy::fromIterator($week, function ($left, $right) {
                return strcmp($left[0]->datetime, $right[0]->datetime);
            }),
        ];
        ok($res, render($app, 'front.phtml', $vars));
        break;

    case $http->GET('/by-month'):
        $tz = new DateTimeZone("Asia/Manila");
        $first = new DateTime("first day of this month", $tz);
        $now = new DateTime("now", $tz);
        $start = $first->format("Y-m-d");
        $end = $now->format("Y-m-d");
        $oneMonth = new DateInterval("P1M");
        $month = $app->model->violations->getAllInDateRange($start, $end);
        $vars = [
            'data' => SortBy::fromIterator($month, function ($left, $right) {
                return strcmp($left[0]->datetime, $right[0]->datetime);
            }),
            'header' => "Month of " . $first->format("F Y"),
            'lastMonth' => $first->sub($oneMonth)->format("Y/m"),
        ];
        ok($res, render($app, 'front.phtml', $vars));
        break;

    case $http->GET('/by-month/:year(\d{4})/:month(\d\d)'):
        $tz = new DateTimeZone("Asia/Manila");
        $args = $http->args();
        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July',
                   'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
        $i = min(11, max(0, ((int) $args->month) - 1));
        $month = "{$months[$i]} {$args->year}";
        $first = new DateTime("first day of {$month}", $tz);
        $last = new DateTime("last day of {$month}", $tz);
        $now = new DateTime("first day of this month", $tz);
        $oneMonth = new DateInterval("P1M");
        $start = $first->format("Y-m-d");
        $end = $last->format("Y-m-d");
        $inCurrMonth = $now->diff($first)->days == 0;
        $vars = [
            'data' => $app->model->violations->getAllInDateRange($start, $end),
            'header' => "Month of {$first->format("F Y")}",
            'lastMonth' => $first->sub($oneMonth)->format("Y/m"),
            'nextMonth' => $inCurrMonth ? // note the mutation by sub()
                null : $first->add($oneMonth)->add($oneMonth)->format("Y/m"),
        ];
        ok($res, render($app, 'front.phtml', $vars));
        break;

    case $http->GET('/by-student'):
        $service = $app->model->violations;
        $vars = [
            'data' => $service->getTopViolators(),
            'subTemplate' => '_top.phtml',
        ];
        ok($res, render($app, 'front.phtml', $vars));
        break;

    case $http->GET('/by-student/:code(\d+)'):
        $code = $http->args()->code;
        $service = $app->model->violations;
        $vars = [
            'data' => $service->getByCodeGroupSameDayDuplicates($code),
            'subTemplate' => '_student.phtml',
        ];
        ok($res, render($app, 'front.phtml', $vars));
        break;

    case $http->GET('/by-student/:name'):
        $name = urldecode($http->args()->name);
        $service = $app->model->violations;
        $vars = [
            'data' => $service->getByNameGroupSameDayDuplicates($name),
            'subTemplate' => '_student.phtml',
        ];
        ok($res, render($app, 'front.phtml', $vars));
        break;

    case $http->GET('/search'):
        if ($app->request->query()->tryGet('value', $value) && $value !== '') {
            if (is_numeric($value)) {
                redirect($res, $url("/index.php/by-student/{$value}"));
            } else {
                $svc = $app->model->violations;
                $found = $svc->findNamesLike($value);
                // assert is_array($found)
                if (empty($found)) {
                    ok($res, render($app, 'front.phtml', [
                        'data' => [],
                        'subTemplate' => '_student.phtml',
                    ]));
                } else if (count($found) == 1) {
                    $code = $found[0]['code'];
                    redirect($res, $url("/index.php/by-student/{$code}"));
                } else {
                    ok($res, render($app, 'front.phtml', [
                        'matches' => $found,
                        'subTemplate' => '_foundmany.phtml',
                    ]));
                }
            }
        } else {
            // might be an accidental submit
            $res->setStatus(Http::NO_CONTENT); // act as if nothing happened
        }
        break;

    case $http->match('/index.php/not-found'): // intentional cascade
    default:
        // TODO: template for this
        $res->setStatus(Http::NOT_FOUND)
            ->setHeader(Http::CONTENT_TYPE, 'text/html')
            ->write('<html><head><title>Not found</title></head><body>Not found.</body></html>');
        break;
    }
} catch (Exception $e) {
    // TODO: template for this
    $res->setStatus(Http::INTERNAL_SERVER_ERROR)
        ->setHeader(Http::CONTENT_TYPE, 'text/html')
        ->write('<html><head><title>Error</title></head><body><p>Sorry. Exception was: ' 
            . $e->getMessage() . '</p></body></html>');
}

$res->send();

