<?php

namespace vio\models;

use DateTime,
    DateTimeZone;

class Violation
{
    const DATE_FORMAT = "D, M d H:i:s";
    const DB_DATE_FORMAT = "Y-m-d H:i:s";
    /** @var int */
    public $id;
    /** @var int */
    public $code;
    /** @var string */
    public $name;
    /** @var string */
    public $year;
    /** @var int */
    public $section;
    /** @var string */
    public $datetime;
    /** @var string */
    public $description;
    /** @var string */
    public $security;

    /** @var DateTime */
    protected $dt;
    static public $parse = [__CLASS__, 'parse'];

    /**
     * Violation -> bool
     */
    function isOnSameDay(Violation $that)
    {
        // TODO: maybe disregard the time and compare strictly on date?
        $myDate = $this->getDateTime();
        $otherDate = $that->getDateTime();
        $diff = $otherDate->diff($myDate);
        return $diff->days == 0;
    }

    /**
     * str -> str
     */
    function formatDate($format=self::DATE_FORMAT)
    {
        if (!empty($this->datetime)) {
            return $this->getDateTime()->format($format);
        } else {
            return '';
        }
    }

    /**
     * () -> DateTime
     */
    protected function getDateTime()
    {
        if (empty($this->dt) && !empty($this->datetime)) {
            $tz = new DateTimeZone("Asia/Manila");
            $this->dt = new DateTime($this->datetime, $tz);
        }
        return $this->dt;
    }

    /** str{} -> Violation */
    static function parse(array $data)
    {
        $result = new Violation();
        $result->id = self::maybeInt($data, 'id');
        $result->code = self::maybeInt($data, 'code');
        $result->name = self::maybe($data, 'name');
        $result->year = self::maybe($data, 'year');
        $result->section = self::maybeInt($data, 'section');
        $result->datetime = self::maybe($data, 'datetime') 
                         ?: self::maybe($data, 'timestamp');
        $result->description = self::maybe($data, 'description');
        $result->security = self::maybe($data, 'security')
                         ?: self::maybe($data, 'security_name');
        return $result;
    }

    /** str{}, str, int? -> int */
    static protected function maybeInt(array $data, $col, $default=null)
    {
        if (array_key_exists($col, $data)) {
            return (int) $data[$col];
        } else {
            return $default;
        }
    }

    /** str{}, str, any? -> any */
    static protected function maybe(array $data, $col, $default=null)
    {
        if (array_key_exists($col, $data)) {
            return $data[$col];
        } else {
            return $default;
        }
    }
}

