<?php

namespace vio\support;

class SchemaVersion
{
    protected $version;

    function set($ver)
    {
        $this->version = $ver;
    }

    function compare($ver)
    {
        return $this->version - $ver;
    }

}

