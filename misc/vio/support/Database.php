<?php

namespace vio\support;

use PDO;

class Database
{
    const VERSION = 3;
    /** @var PDO */
    protected $db;

    protected $tables = [
        'data' => [
            'id' => 'INTEGER PRIMARY KEY',
            'code' => 'INT',
            'name' => 'STRING',
            'year' => 'STRING',
            'section' => 'INT',
            'datetime' => 'STRING',
            'description' => 'STRING',
            'security' => 'STRING',
        ],
        'users' => [
            'id' => 'INTEGER PRIMARY KEY',
            'username' => 'STRING',
            'password' => 'STRING',
            'last_auth' => 'STRING',
        ],
    ];

    protected $indexes = [
        'student_code' => ['data', ['code']],
        'student_name' => ['data', ['name']],
        'date_entered' => ['data', ['datetime', 'security']],
        'user_name' => ['users', ['username']],
    ];

    /**
     * PDO, SchemaVersion? -> ()
     */
    function __construct(PDO $db, SchemaVersion $version=null)
    {
        $this->db = $db;
        if (empty($version) || $version->compare(self::VERSION) < 0) {
            $this->updateSchema($db);
        }
    }

    /** 
     * str -> str 
     */
    static function identifier($str)
    {
        $quoted = str_replace('"', '""', $str);
        return "\"{$quoted}\"";
    }

    /**
     * str, any... -> PDOStatement 
     */
    function exec($query, $var_args=null)
    {
        $args = array_slice(func_get_args(), 1);
        $statement = $this->db->prepare($query);
        if (empty($statement)) {
            $info = $this->db->errorInfo();
            throw new \RuntimeException(
                "Problem with query. DB says '{$info[2]}'.");
        }
        $statement->execute($args);
        return $statement;
    }

    /**
     * () -> DbSession
     */
    function begin()
    {
        return new DbSession($this->db);
    }

    /** PDO -> () */
    protected function updateSchema(PDO $db)
    {
        $db->beginTransaction();
        foreach ($this->tables as $table => $columns) {
            $cols = [];
            foreach ($columns as $col => $type) {
                $cols[] = self::identifier($col) . " {$type}";
            }
            $tbl = self::identifier($table);
            $db->exec("DROP TABLE IF EXISTS {$tbl};\n"
                . "CREATE TABLE {$tbl} (\n  " . implode(",\n  ", $cols) . ');');
        }
        foreach ($this->indexes as $name => $index) {
            list($tbl, $cols) = $index;
            $idx = self::identifier($name);
            $db->exec("DROP INDEX IF EXISTS {$idx};\n"
                . "CREATE INDEX {$idx} ON " . self::identifier($tbl) . " ("
                . implode(', ', array_map([__CLASS__, 'identifier'], $cols))
                . ');');
        }
        $db->commit();
    }
}

