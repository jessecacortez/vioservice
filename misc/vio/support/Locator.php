<?php

namespace vio\support;

class Locator extends \Bad_Di_Locator
{
    function __get($name)
    {
        return $this->get($name);
    }
}

