<?php

namespace vio\support\view;

use Plink\View\Widget as Base,
    Plink\View\IRenderer,
    Plink\View\IView,
    Plink\View\Node\Value,
    Plink\View\Node\ElementList;

class Widget extends Base
{
    /**
     * any... -> Widget<ElementList>
     */
    function bare($elements)
    {
        $siblings = new ElementList();
        $elements = func_get_Args();
        foreach ($elements as $el) {
            $siblings->add(Value::unit($el));
        }
        return $this->wrap($siblings);
    }

    /**
     * IView -> Widget
     */
    function wrap(IView $view)
    {
        $inst = new $this;
        $inst->node = $view instanceof Base ? $view->node : $view;
        return $inst;
    }

    /**
     * IRenderer, any{}? -> str
     */
    function using(IRenderer $renderer, array $data=[])
    {
        return $renderer->render($this->transform($data));
    }

}

