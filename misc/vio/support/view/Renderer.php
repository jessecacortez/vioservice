<?php

namespace vio\support\view;

use Plink\View\Renderer\Html,
    Bad_Struct_PathMap as PathMap,
    BadMethodCallException;

class Renderer extends Html
{
    protected $paths;
    protected $__ENV;

    function __construct(Resolver $resolver)
    {
        $this->paths = $resolver;
    }

    /**
     * str, any{} -> str
     */
    function renderTemplate($tpl, array $env)
    {
        if ($this->paths->tryFind($tpl, $file)) {
            $this->__ENV = new PathMap($env);
            return $this->reallyRender($file);
        } else {
            // TODO: should throw
            return '<html><head><title>not found.</title></head><body><p>No template named '
                . $tpl . ' in search path.</p></body></html>';
        }
    }

    /**
     * str, any{} -> str
     */
    function renderPartial($tpl, array $env=[])
    {
        $newEnv = !empty($this->__ENV) ? $env + $this->__ENV->flatten() : $env;
        $partial = new self($this->paths);
        return $partial->renderTemplate($tpl, $newEnv);
    }

    /**
     * str -> str
     */
    function escape($str)
    {
        return $this->quote($str);
    }

    /**
     * str -> any
     */
    function __get($name)
    {
        return !empty($this->__ENV) ? $this->__ENV->get($name) : null;
    }

    /**
     * str -> bool
     */
    function __isset($name)
    {
        return !empty($this->__ENV) && $this->__ENV->has($name);
    }

    /**
     * str, any[] -> any
     */
    function __call($name, array $args)
    {
        $obj = $this->__get($name);
        if (!empty($obj) && is_callable($obj)) {
            return call_user_func_array($obj, $args);
        } else {
            throw new BadMethodCallException("no such method {$name}");
        }
    }

    /**
     * str -> str
     */
    protected function reallyRender($__FILE)
    {
        ob_start();
        include $__FILE;
        return ob_get_clean();
    }
}

