<?php

namespace vio\support\view;

class Bootstrap extends Widget
{
    const NAVBAR = 'genNavbar';
    const NAV = 'genNav';
    const TWO_PANE = 'genTwoPane';

    static protected $memo = [];

    /**
     * str, Widget -> IView
     */
    static function memoize($generator, Widget $widget)
    {
        if (empty(self::$memo[$generator])) {
            self::$memo[$generator] = 
                call_user_func([get_called_class(), $generator], $widget);
        }
        return self::$memo[$generator];
    }

    /**
     * Widget -> Widget<Element--div.navbar>
     */
    static function genNavbar(Widget $w)
    {
        return $w->div([
            'class' => 'navbar navbar-inverse navbar-fixed-top', 
            'role' => 'navigation'
        ],
            $w->div(['class' => 'container-fluid'],
                $w->div(['class' => 'navbar-header'],
                    $w->button([
                        'type' => 'button', 
                        'class' => 'navbar-toggle',
                        'data-toggle' => 'collapse',
                        'data-target' => '.navbar-collapse',
                    ], 
                        $w->span(['class' => 'sr-only'], 'Toggle navigation'),
                        $w->span(['class' => 'icon-bar']),
                        $w->span(['class' => 'icon-bar']),
                        $w->span(['class' => 'icon-bar'])
                    ),
                    $w->when($w->brandUrl,
                        $w->a([
                            'class' => 'navbar-brand', 
                            'href' => $w->brandUrl,
                        ], $w->brand),
                        $w->span(['class' => 'navbar-brand'], $w->brand)
                    )
                ),
                $w->div(['class' => 'navbar-collapse collapse'],
                    $w->navigation,
                    $w->both($w->navbarForm,
                        $w->form([
                            'class' => 'navbar-form navbar-right',
                            'name' => $w->navbarFormName,
                            'action' => $w->navbarFormAction,
                        ],
                            $w->navbarForm))
                )
            )
        );
    }

    /**
     * Widget -> Widget@(Element, ul.nav)
     */
    static function genNav(Widget $w)
    {
        $li = $w->li(
            $w->when($w->navActive->eq($w->url), $w->attrib('class', 'active')),
            $w->a(['href' => $w->either($w->url, '#')], $w->item));
        $ul = $w->ul(['class' => $w->either($w->navClass, 'nav')],
            $w->navItems->map($li));
        return $ul;
    }

    /**
     * Widget -> Widget@(Element, div.container-fluid)
     */
    static function genTwoPane(Widget $w)
    {
        return $w->div(['class' => 'container-fluid'],
            $w->div(['class' => 'row'],
                $w->div(['class' => $w->leftClass], $w->left),
                $w->div(['class' => $w->rightClass], $w->right)
            )
        );
    }

    /**
     * str, {str brandUrl?, Element navbarForm?} -> Bootstrap@(Element, div.navbar)
     */
    function navbar($brand, array $opts=[])
    {
        $graph = static::memoize(self::NAVBAR, $this);
        $opts['brand'] = $brand;
        return $this->wrap($graph($opts));
    }

    /**
     * str{navClass?, navActive?, {url, item}[] navItems} -> Bootstrap@(Element, ul.nav)
     */
    function nav(array $items)
    {
        $graph = static::memoize(self::NAV, $this);
        return $this->wrap($graph($items));
    }

    /**
     * str, str, IView{left, right} -> Bootstrap@(Element, div.container-fluid)
     */
    function twoPaneView($leftCls, $rightCls, array $contents=[])
    {
        // TODO: specify only widths, generate classes from those
        $graph = static::memoize(self::TWO_PANE, $this);
        $contents['leftClass'] = $leftCls;
        $contents['rightClass'] = $rightCls;
        return $this->wrap($graph($contents));
    }
}

