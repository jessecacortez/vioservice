<?php

namespace vio\support\view;

use Plink\View\IRenderable,
    Plink\View\IRenderer;

class Template implements IRenderable
{
    protected $tpl;
    protected $vars;

    /**
     * str -> ()
     */
    function __construct($tpl)
    {
        $this->tpl = $tpl;
    }

    /**
     * any{}? -> IRenderable
     */
    function transform(array $data=[])
    {
        $this->vars = $data;
        return $this;
    }

    /**
     * IRenderer -> str
     */
    function render(IRenderer $renderer)
    {
        if ($renderer instanceof Renderer) {
            return $renderer->renderTemplate($this->tpl, $this->vars);
        } else {
            // TODO: throw?
            return '';
        }
    }
}

