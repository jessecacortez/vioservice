<?php

namespace vio\support\view;

use Bad_Struct_PathMap as Map;

class Resolver
{
    protected $paths = [];
    protected $map;

    function __construct()
    {
        // $this->map = new Map();
    }

    /**
     * str -> Resolver
     */
    function add($path)
    {
        $this->paths[] = rtrim($path, "/");
        return $this;
    }

    /**
     * str, &str -> bool
     */
    function tryFind($subpath, &$OUT)
    {
        $sub = ltrim($subpath, "/");
        foreach ($this->paths as $prefix) {
            $test = "{$prefix}/{$sub}";
            if (file_exists($test)) {
                // TODO: cache
                $OUT = $test;
                return true;
            }
        }
        return false;
    }
}

