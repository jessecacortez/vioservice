<?php

namespace vio\support\view;

/**
 * Handles output buffering for the script.
 *
 */
class Capture
{
    protected $initial = array();
    protected $last;
    protected $out = array();

    function __construct($content=null)
    {
        $this->last = $content;
    }

    /**
     * int -> Capture
     */
    function start($levels=1)
    {
        do {
            $this->initial[] = ob_get_level();
            ob_start();
        } while (--$levels > 0);
        return $this;
    }

    /**
     * () -> &str
     */
    function &toRef()
    {
        $this->start();
        $count = count($this->initial);
        $this->out[$count] = '';
        return $this->out[$count];
    }

    /**
     * &int -> str
     */
    function end(&$diff = null)
    {
        if (empty($this->initial)) {
            return '';
        }
        $level = ob_get_level();
        $count = count($this->initial);
        $init = array_pop($this->initial);
        $diff = $level - $init;
        $content = '';
        while ($level-- > $init) {
            $content .= ob_get_clean();
        }
        $this->last = $content;
        if (array_key_exists($count, $this->out)) {
            $this->out[$count] = $content; 
            // need this to make sure future captures won't point
            // to the same thing, causing previous captures to change.
            unset($this->out[$count]); 
        }
        return $content;
    }

    /**
     * () -> str
     */
    function retrieve()
    {
        return $this->last;
    }
    
    /**
     * () -> bool
     */
    function isEmpty()
    {
        return empty($this->last);
    }

    /**
     * callable? -> str
     */
    function __invoke(callable $fn=null)
    {
        if (!empty($fn)) {
            $this->start();
            call_user_func($fn);
            $output = $this->end($diff);
            if ($diff != 1) {
                trigger_error('Imbalanced output levels!', E_USER_WARNING);
            }
            return $output;
        } else {
            return $this->last;
        }
    }
}
