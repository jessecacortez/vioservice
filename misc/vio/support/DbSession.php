<?php

namespace vio\support;

use PDO;

class DbSession
{
    /** @var PDO */
    protected $db;
    /** @var bool */
    protected $success = true;
    /** @var bool */
    protected $inProgress = true;

    function __construct(PDO $db)
    {
        $this->db = $db;
        if (!$db->inTransaction()) {
            $db->beginTransaction();
        }
    }

    /** () -> () */
    function __destruct()
    {
        $this->end();
    }

    /** str, any{} -> bool */
    function insert($table, array $data)
    {
        $rows = [array_keys($data), array_values($data)];
        return $this->insertMany($table, $rows);
    }

    /** str, any{}[] -> bool */
    function insertMany($table, array $data)
    {
        $columns = array_shift($data);
        $cols = implode(', ', $columns);
        $vals = implode(', ', array_fill(0, count($columns), '?'));
        $sql = 'INSERT INTO ' . Database::identifier($table) . " ({$cols})\n"
             . "VALUES ({$vals});";
        $stmt = $this->db->prepare($sql);
        foreach ($data as $values) {
            $res = $stmt->execute(array_values($values));
        }
        return $res;
    }
    
    /** str, any[] -> PDOStatement */
    function query($sql, array $args=[])
    {
        $stmt = $this->db->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }

    /** () -> () */
    function failed()
    {
        $this->success = false;
    }

    /** () -> bool */
    function end()
    {
        if (!$this->inProgress) {
            return false;;
        }
        if ($this->success) {
            $this->db->commit();
        } else {
            $this->db->rollBack();
        }
        $this->inProgress = false;
        return $this->success;
    }

}

