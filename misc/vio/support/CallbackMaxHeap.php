<?php

namespace vio\support;

use SplMaxHeap,
    Traversable;

class CallbackMaxHeap extends SplMaxHeap
{
    protected $proc;

    /**
     * (T, T -> int) -> () @(positive means left is greater)
     */
    function __construct(callable $comparator)
    {
        $this->proc = $comparator;
    }

    /**
     * T, T -> int
     */
    function compare($left, $right)
    {
        $compare = $this->proc;
        return $compare($left, $right);
    }

    /**
     * T[], (T, T -> int) -> CallbackMaxHeap
     */
    static function fromIterator(Traversable $it, callable $comparator)
    {
        $heap = new static($comparator);
        foreach ($it as $elem) {
            $heap->insert($elem);
        }
        return $heap;
    }
}

