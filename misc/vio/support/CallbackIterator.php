<?php

namespace vio\support;

use IteratorIterator,
    Traversable;

class CallbackIterator extends IteratorIterator
{
    /** @var callable */
    protected $proc;

    /**
     * Traversable, callable -> ()
     */
    function __construct(Traversable $iterator, callable $proc)
    {
        $this->proc = $proc;
        parent::__construct($iterator);
    }

    /**
     * () -> any
     */
    function current()
    {
        $fn = $this->proc;
        return $fn(parent::current());
    }
}

