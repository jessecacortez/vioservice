<?php

namespace vio\services;

use vio\models\Violation as Model,
    vio\support\Database,
    vio\support\DbSession,
    vio\support\CallbackIterator as Map,
    vio\support\GroupingIterator as Group,
    vio\support\CallbackMaxHeap as Sorted,
    Bad_Debug as Debug,
    PDO,
    PDOException,
    ArrayIterator,
    EmptyIterator;

class Violation
{
    const ALLOWED_DUPES = 3;
    protected $db;

    /**
     * Database -> ()
     */
    function __construct(Database $db)
    {
        $this->db = $db;
    }

    /**
     * () -> str{datetime, security}|{bool isEmpty}
     */
    function getLastEntry()
    {
        $query = "SELECT datetime, security FROM data ORDER BY id DESC LIMIT 1";
        $result = $this->db->exec($query)->fetch(PDO::FETCH_ASSOC);
        return $result === false ? ['isEmpty' => true] : $result;
    }

    /**
     * () -> {int code, str name, int count}[]
     */
    function getTopViolators()
    {
        $sql = "SELECT code, name, datetime, description FROM data "
             . "ORDER BY code, description, datetime;";
        $result = $this->db->exec($sql);
        $distinct = $this->groupSameDayDupes(new Map($result, Model::$parse));
        $takeFirst = new Map($distinct, function (array $group) {
            return $group[0];
        });
        $byStudent = new Group($takeFirst, function (Model $ref, Model $curr) {
            return $ref->code == $curr->code ? 0 : 1;
        });
        $countViolations = new Map($byStudent, function (array $group) {
            $vio = $group[0];
            return ['code' => $vio->code, 'name' => $vio->name,
                    'count' => count($group)];
        });
        // generator is fully consumed here. is there a better way?
        // TODO: if you ever plan on implementing caching, do it here first.
        return Sorted::fromIterator($countViolations, function ($left, $right) {
            return $left['count'] - $right['count'];
        });
    }

    /**
     * str -> Model[]
     */
    function getAllByName($name)
    {
        $query = "SELECT * FROM data WHERE name = ?";
        $results = $this->db->exec($query, $name);
        return new Map($results, Model::$parse);
    }

    /**
     * int -> Model[]
     */
    function getAllByCode($code)
    {
        $query = "SELECT * FROM data WHERE code = ?";
        $results = $this->db->exec($query, (int) $code);
        return new Map($results, Model::$parse);
    }

    /**
     * str, str -> Model[]
     */
    function getAllSinceAndBySecurity($datetime, $security)
    {
        // session to lock because i need two queries
        $session = $this->db->begin();
        $preQuery = "SELECT id FROM data WHERE datetime=? AND security=?";
        $id = $session->query($preQuery, [$datetime, $security])->fetchColumn(0);
        if ($id !== false) {
            $sql = "SELECT * FROM data WHERE id > ?";
            $rows = $session->query($sql, [(int) $id]);
            $results = new Map($rows, Model::$parse);
        } else {
            $results = new EmptyIterator();
        }
        $session->end();
        return $results;
    }

    /**
     * str, int -> Model[][]
     */
    function getByNameGroupSameDayDuplicates($name, $max=self::ALLOWED_DUPES)
    {
        $sql = "SELECT * FROM data WHERE name=? ORDER BY code, description, datetime;";
        $rows = $this->db->exec($sql, $name);
        return $this->groupSameDayDupes(new Map($rows, Model::$parse), $max);
    }

    /**
     * int,  int -> Model[][]
     */
    function getByCodeGroupSameDayDuplicates($code, $max=self::ALLOWED_DUPES)
    {
        $sql = "SELECT * FROM data WHERE code=? ORDER BY code, description, datetime;";
        $rows = $this->db->exec($sql, (int) $code);
        return $this->groupSameDayDupes(new Map($rows, Model::$parse), 
            $max);
    }

    /**
     * str, str -> Model[]
     */
    function getAllInDateRange($low, $high)
    {
        // need to be careful with string comparisons here. not sure if
        // this is correct.
        $sql = "SELECT * FROM data WHERE datetime > ? AND datetime < ? "
             . " ORDER BY code, description, datetime DESC";
        $rows = $this->db->exec($sql, "{$low} 00:00:00", "{$high} 23:59:59");
        return $this->groupSameDayDupes(new Map($rows, Model::$parse), 3);
    }

    /**
     * Model, DbSession? -> bool
     */
    function put(Model $data, DbSession $session=null)
    {
        $shouldEnd = false;
        if (empty($session)) {
            $session = $this->db->begin();
            $shouldEnd = true;
        }
        try {
            $result = $session->insert('data', [
                'code' => $data->code,
                'name' => $data->name,
                'year' => $data->year,
                'section' => $data->section,
                'datetime' => $data->datetime,
                'description' => $data->description,
                'security' => $data->security,
            ]);
        } catch (PDOException $e) {
            $session->failed();
            if ($shouldEnd) {
                $session->end();
            }
            throw $e;
        }
        if ($shouldEnd) {
            $session->end();
        }
        return $result;
    }

    /**
     * Model[], DbSession? -> bool
     */
    function putMany($data, DbSession $session=null)
    {
        $shouldEnd = false;
        if (empty($session)) {
            $session = $this->db->begin();
            $shouldEnd = true;
        }
        try {
            $rows = [['code', 'name', 'year', 'section', 'datetime', 
                      'description', 'security']];
            foreach ($data as $vio) {
                $rows[] = [$vio->code, $vio->name, $vio->year, $vio->section,
                           $vio->datetime, $vio->description, $vio->security];
            }
            $result = $session->insertMany('data', $rows);
        } catch (PDOException $e) {
            $session->failed();
            if ($shouldEnd) {
                $session->end();
            }
            throw $e;
        }
        if ($shouldEnd) {
            $session->end();
        }
        return $result;
    }

    /**
     * str -> {int code, str name}[]
     */
    function findNamesLike($name)
    {
        $name = trim($name);
        $sql = "SELECT code, name FROM data WHERE name LIKE ? GROUP BY code";
        $results = $this->db->exec($sql, "%{$name}%");
        return $results->fetchAll(PDO::FETCH_ASSOC);
    }

    /** str{}[] -> Model[] */
    function toEntityCollection($rows)
    {
        if (is_array($rows)) {
            $rows = new ArrayIterator($rows);
        }
        return new Map($rows, Model::$parse);
    }

    /** 
     * Model[], int -> Model[][] 
     * only works when sorted by code, description, then datetime
     */
    function groupSameDayDupes($rows, $max=self::ALLOWED_DUPES)
    {
        if (is_array($rows)) {
            $rows = new ArrayIterator($rows);
        }
        $counter = 0;
        return new Group($rows, function (Model $ref, Model $curr) 
                                use (&$counter, $max) {
            // same student, same violation
            $isDupe = $ref->code == $curr->code 
                && $ref->description == $curr->description;
            if ($isDupe && $ref->isOnSameDay($curr)) {
                return ++$counter % $max == 0 ? 1 : 0;
            } else {
                $counter = 0;
                return 1;
            }
        });
    }

}

