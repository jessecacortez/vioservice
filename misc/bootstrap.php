<?php

$lib = realpath(__DIR__ . "/../libs");

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_Errors', 1);


require_once $lib . "/bad/Bad/Loader.php";

Bad_Loader::autoload();

$plinkLoader = new Bad_Loader("Plink", $lib . "/plink/src");
$plinkLoader->setSeparator("\\")->register();

$appLoader = new Bad_Loader("vio", __DIR__);
$appLoader->setSeparator("\\")->register();

return new Bad_Application([
    'router' => 'Bad_Http_Router',
    'request' => 'Bad_Http_Request',
    'response' => 'Bad_Http_Response',
    'model' => [
        'factory' => function () {
            return new vio\support\Locator([
                'violations' => 'vio\services\Violation',
                'vio\support\SchemaVersion' => [
                    'invoke' => [['set', 3]],
                ],
                'PDO' => [
                    'factory' => function () {
                        return new PDO("sqlite:" 
                            . __DIR__ . "/db/violations.sqlite");
                    },
                ],
            ]);
        },
    ],
    'view' => [
        'factory' => function () {
            return new vio\support\Locator([
                'renderer' => 'vio\support\view\Renderer',
                'vio\support\view\Resolver' => [
                    'invoke' => [['add', __DIR__ . "/templates"]],
                ],
            ]);
        },
    ],
    'url' => 'vio\support\Url',
]);

