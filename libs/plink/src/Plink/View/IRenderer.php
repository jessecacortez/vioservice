<?php

namespace Plink\View;

use Plink\View\Node\Value;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Should contain methods to evaluate the guest languages primitive values.
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
interface IRenderer
{
    /**
     * IView, any{} -> str
     * 
     * Evaluate view then render
     * 
     * @param IView $view
     * @param array $data
     * @return string
     */
    function render(IView $view, array $data=[]);

    /**
     * any -> str
     * 
     * @param mixed $value
     * @return string
     */
    function renderValue($value);

    /**
     * str, A[], IRenderable[] -> str
     * 
     * @param string type
     * @param array $attribs
     * @param array $children
     * @return string
     */
    function renderElement($type, array $attribs, array $children);

    /**
     * IRenderable[] -> str
     * 
     * @param array $list Should contain IRenderables only.
     * @return str
     */
    function renderElementList(array $list);

    /**
     * Value, Value -> str
     * 
     * @param Value $name
     * @param Value $value
     * @return string
     */
    function renderAttribute(Value $name, Value $value);

    /**
     * A[] -> str : A := Attribute | AttributeList
     * 
     * @param array $list Should contain Attributes and AttributeLists only.
     * @return string
     */
    function renderAttributeList(array $list);

    /**
     * str -> str
     * 
     * @param string $html
     * @return string
     */
    function renderHtmlFragment($html);

    /**
     * any -> str
     * 
     * @param mixed $value
     * @return string
     */
    function quote($value);
}
