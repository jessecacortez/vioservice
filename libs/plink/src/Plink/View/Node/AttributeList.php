<?php

namespace Plink\View\Node;

use Plink\View\IView,
    Plink\View\IRenderable,
    Plink\View\IRenderer;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of AttributeList
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class AttributeList implements IRenderable
{
    protected $members = [];

    /** 
     * IView, IView -> ()
     * IView -> ()
     * 
     * @param \Plink\View\IView $value
     * @param \Plink\View\IView $name
     */
    function add(IView $value, IView $name=null)
    {
        if (!empty($name)) {
            $this->members[] = new Attribute($name, $value);
        } else {
            $this->members[] = $value;
        }
    }

    /** any{} -> IRenderer */
    function transform(array $data=[])
    {
        $reduced = new AttributeList();
        foreach ($this->members as $member) {
            $value = $member->transform($data);
            if (!$value instanceof Attribute 
                    && !$value instanceof AttributeList) {
                throw new \UnexpectedValueException(
                    "Bad AttributeList: every member should evaluate into an " . 
                    "Attribute or AttributeList.");
            }
            $reduced->add($value);
        }
        return $reduced;
    }

    /** IRenderer -> str */
    function render(IRenderer $renderer)
    {
        return $renderer->renderAttributeList($this->members);
    }
}
