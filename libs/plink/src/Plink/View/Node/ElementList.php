<?php

namespace Plink\View\Node;

use Plink\View\IView,
    Plink\View\IRenderable,
    Plink\View\IRenderer,
    IteratorAggregate,
    ArrayIterator;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of ElementList
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class ElementList implements IRenderable, IteratorAggregate
{
    protected $members = [];

    /** IView -> () */
    function add(IView $member)
    {
        $this->members[] = $member;
    }

    /** any{} -> IRenderable */
    function transform(array $data=[])
    {
        $reduced = new ElementList();
        foreach ($this->members as $member) {
            $reduced->add($member->transform($data));
        }
        return $reduced;
    }

    /** IRenderer -> str */
    function render(IRenderer $renderer)
    {
        return $renderer->renderElementList($this->members);
    }

    /**
     * () -> ArrayIterator
     * 
     * @return ArrayIterator
     */
    function getIterator()
    {
        return new ArrayIterator($this->members);    
    }

}
