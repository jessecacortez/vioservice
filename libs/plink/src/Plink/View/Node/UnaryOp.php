<?php

namespace Plink\View\Node;

use Plink\View\IView,
    Plink\View\IRenderable;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of UnaryOp
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
abstract class UnaryOp implements IView
{
    protected $operand;

    function __construct(IView $operand)
    {
        $this->operand = $operand;
    }

    /** any{} -> IRenderable */
    function transform(array $data=[])
    {
        return $this->apply($this->operand->transform($data))->transform($data);
    }

    /** IRenderable -> IRenderable */
    abstract function apply(IRenderable $operand);
}
