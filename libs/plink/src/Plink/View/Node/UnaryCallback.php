<?php

namespace Plink\View\Node;

use Plink\View\IView,
    Plink\View\IRenderable;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of UnaryCallback
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class UnaryCallback extends UnaryOp
{
    protected $fn;

    function __construct(IView $operand, callable $fn)
    {
        $this->operand = $operand;
        $this->fn = $fn;
    }

    /** IRenderable -> IRenderable */
    function apply(IRenderable $operand)
    {
        $fn = $this->fn;
        if ($operand instanceof Value) {
            $operand = $operand->get();
        }
        return Value::unit($fn($operand));
    }
}
