<?php

namespace Plink\View\Node;

use Plink\View\IView,
    Plink\View\IRenderable,
    Plink\View\IRenderer;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Attribute
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class Attribute implements IRenderable
{
    protected $name;
    protected $value;

    function __construct(IView $name, IView $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /** any{} -> IRenderable */
    function transform(array $data=[])
    {
        return new Attribute($this->name->transform($data), 
            $this->value->transform($data));
    }

    /** IRenderer -> str */
    function render(IRenderer $renderer)
    {
        if ($this->value instanceof Attribute 
                || $this->value instanceof AttributeList) {
            return $this->value->render($renderer);
        } else if (!$this->value instanceof Value) {
            throw new \UnexpectedValueException(
                "Bad Attribute: value must evaluate to an Attribute or Value.");
        }
        if (!$this->name instanceof Value) {
            throw new \UnexpectedValueException(
                "Bad Attribute: name must evaluate to a Value.");
        }
        return $renderer->renderAttribute($this->name, $this->value);
    }
}
