<?php

namespace Plink\View\Node;

use Plink\View\IRenderable,
    Plink\View\IRenderer;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of HtmlFragment
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class HtmlFragment implements IRenderable
{
    protected $value;

    function __construct($html)
    {
        $this->value = $html;
    }

    /** any{} -> IRenderable */
    function transform(array $data=[])
    {
        return $this;
    }

    /** IRenderer -> str */
    function render(IRenderer $renderer)
    {
        return $renderer->renderHtmlFragment($this->value);
    }

}
