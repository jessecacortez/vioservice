<?php

namespace Plink\View\Node;

use Plink\View\IView,
    Plink\View\IRenderable;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of When
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class When implements IView
{
    protected $if;
    protected $then;
    protected $else;

    function __construct(IView $if, IView $then, IView $else=null)
    {
        $this->if = $if;
        $this->then = $then;
        $this->else = $else;
    }

    /** any{} -> IRenderable */
    function transform(array $data=[])
    {
        $cond = $this->if->transform($data);
        if (self::isTruthy($cond)) {
            if ($this->then === $this->if) {
                return $cond;
            } else {
                return $this->then->transform($data);
            }
        } else {
            if ($this->else === $this->if) {
                return $cond;
            } else if ($this->else !== null) {
                return $this->else->transform($data);
            } else {
                return new Value(null);
            }
        }
    }

    /** IRenderable -> bool */
    static function isTruthy(IRenderable $expr)
    {
        if ($expr instanceof Value) {
            return $expr->get();
        } else {
            return true;
        }
    }

}
