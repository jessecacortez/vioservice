<?php

namespace Plink\View\Renderer;

use Plink\View\IRenderer,
    Plink\View\IView,
    Plink\View\Node\Value;


/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Html
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
class Html implements IRenderer
{
    public $tab = "  ";
    public $indentLevel = 0;

    /** 
     * str -> str 
     * 
     * @param string $value
     * @return string
     */
    function quote($value)
    {
        return htmlspecialchars($value);
    }

    /** 
     * IView, any{} -> str
     * IView -> str
     * 
     * @param IView $view
     * @param array $data
     * @return string
     */
    function render(IView $view, array $data=[])
    {
        return $view->transform($data)->render($this);
    }

    /**
     * View, View -> str
     * 
     * @param Value $name
     * @param Value $value
     * @return string
     */
    function renderAttribute(Value $name, Value $value)
    {
        return " " . $name->render($this) . '="' . $value->render($this) . '"';
    }

    /**
     * A[] -> str : A := Attribute | AttributeList
     * 
     * @param array $list Should contain Attributes and AttributeLists only
     * @return string
     */
    function renderAttributeList(array $list)
    {
        $out = "";
        foreach ($list as $attrib) {
            $out .= $attrib->render($this);
        }
        return $out;
    }

    /**
     * str, A[], IRenderable[] -> str : A := Attribute | AttributeList
     * 
     * @param string $type
     * @param array $attribs
     * $param array $children
     * @return string
     */
    function renderElement($type, array $attribs, array $children)
    {
        $type = $this->quote($type);
        $out = "<${type}";
        $out .= $this->renderAttributeList($attribs);
        if ($this->isVoid($type)) {
            $out .= " />";
        } else {
            if (empty($children)) {
                $out .= "></{$type}>";
            } else {
                $out .= ">\n";
                $this->indentLevel++;
                $out .= $this->indent($this->renderElementList($children));
                $this->indentLevel--;
                $out .= "\n" . $this->indent("</{$type}>");
            }
        }
        return $out;
    }

    /**
     * IRenderable[] -> str
     * 
     * @param array $list Should contain IRenderables only
     * @return string
     */
    function renderElementList(array $list)
    {
        $out = "";
        foreach ($list as $child) {
            $out .= $this->indent($child->render($this)) . "\n";
        }
        return trim($out);
    }

    /**
     * str -> str
     * 
     * @param string $html
     * @return string
     */
    function renderHtmlFragment($html)
    {
        // TODO: fix indent
        return $html;
    }

    /**
     * any -> str
     * 
     * @param mixed $value
     * @return string
     */
    function renderValue($value)
    {
        return $this->quote($value);
    }

    /**
     * str -> bool
     * 
     * @param string $type
     * @return bool
     */
    protected function isVoid($type)
    {
        $tag = strtolower($type);
        return in_array($tag, [
            "img", "link", "meta", "hr", "br", "input",
        ]);
    }

    /**
     * str -> str
     * 
     * @param string $str
     * @return string
     */
    protected function indent($str)
    {
        $indent = str_repeat($this->tab, $this->indentLevel);
        return $indent . $str;
    }

}
