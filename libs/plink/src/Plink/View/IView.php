<?php

namespace Plink\View;

/*
 * This file is a part of the Plink-View project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of IView
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2014 Mon Zafra
 * @package    Plink-View
 * @subpackage 
 * @license    MIT License
 */
interface IView
{
    /**
     * any{} -> IRenderable
     * 
     * Evaluate this object into something renderable.
     * 
     * @param array $data
     * @return IRenderable
     */
    function transform(array $data=[]);
}
