<?php

interface IView {
    /** any{} -> IRenderable */
    function evaluate(array $env=[]);
}

interface IRenderable extends IView {
    /** IRenderer -> str */
    function render(IRenderer $renderer);
}

interface IRenderer {
    /** IView, any{} -> str */
    function render(IView $view, array $env=[]);
    /** any -> str */
    function renderValue($value);
    /** str, ElementList -> str */
    function renderElement($type, ElementList $children);
    /** IRenderable[] -> str */
    function renderElementList(array $elements);
    /** IRenderable, IRenderable -> str */
    function renderAttribute(Value $key, Value $val);
    /** Attribute[] -> str */
    function renderAttributeList(array $attributes);
}

class HtmlRenderer implements IRenderer {
    public $tab = "  ";
    public $indentLevel = 0;
    function render(IView $view, array $env=[]) {
        return $view->evaluate($env)->render($this);
    }
    function renderValue($val) {
        return $this->escape($val);
    }
    function renderElement($type, ElementList $children) {
        $tag = $this->escape($type);
        $out = "<{$tag}";
        $closed = false;
        $this->indentLevel++;
        foreach ($children as $el) {
            if ($el instanceof Attribute) {
                $out .= $el->render($this);
            } else {
                if (!$closed) {
                    $closed = true;
                    $out .= ">\n";
                }
                $out .= $this->indent($el->render($this)) . "\n";
            }

        }
        $this->indentLevel--;
        $out .= $this->indent("</{$tag}>");
        return $out;
    }
    function renderElementList(array $elements) {
        $out = "";
        foreach ($elements as $el) {
            $out .= $this->indent($el->render($this)) . "\n";
        }
        return trim($out);
    }
    function renderAttribute(Value $name, Value $value) {
        $name = $name->render($this);
        if (!empty($name)) {
            return " " . $name . '="' . $value->render($this) . '"';
        }
    }
    function renderAttributeList(array $attrs) {
        $out = "";
        foreach ($attrs as $attr) {
            $out .= $attr->render($this);
        }
        return $out;
    }
    function escape($text) {
        return htmlspecialchars($text);
    }
    function indent($text) {
        $tabs = str_repeat($this->tab, $this->indentLevel);
        return "{$tabs}{$text}";
    }
}

class Value implements IRenderable {
    public $value;
    function __construct($value) {
        $this->value = $value;
    }
    static function unit($value) {
        if (!$value instanceof IView) {
            return new self($value);
        } else {
            return $value;
        }
    }
    function render(IRenderer $renderer) {
        return $renderer->renderValue($this->value);
    }
    function evaluate(array $env=[]) {
        return $this;
    }
}

class Name implements IView {
    public $name;
    function __construct($name) {
        $this->name = $name;
    }
    function evaluate(array $env=[]) {
        if (array_key_exists($this->name, $env)) {
            return Value::unit($env[$this->name]);
        } else {
            return new Value(null);
        }
    }
}

class Apply implements IView {
    public $fn;
    public $args;
    function __construct(callable $fn, array $args=[]) {
        $this->fn = $fn;
        $this->args = $args;
    }
    function evaluate(array $env=[]) {
        $args = [];
        foreach ($this->args as $arg) {
            $a = $arg instanceof IView ? $arg->evaluate($env) : $arg;
            $args[] = $a instanceof Value ? $a->value : $a;
        }
        $res = Value::unit(call_user_func_array($this->fn, $args));
        return $res->evaluate($env);
    }
}

class Let implements IView {
    public $name;
    public $value;
    public $expr;
    function __construct($name, IView $value, IView $expr) {
        $this->name = $name;
        $this->value = $value;
        $this->expr = $expr;
    }
    function evaluate(array $env=[]) {
        $env[$this->name] = $this->value->evaluate($env);
        return $this->expr->evaluate($env);
    }
}

class When implements IView {
    public $if;
    public $then;
    public $else;
    function __construct(IView $if, IView $then, IView $else=null) {
        $this->if = $if;
        $this->then = $then;
        $this->else = $else;
    }
    function evaluate(array $env=[]) {
        $cond = $this->if->evaluate($env);
        if ($this->isTruthy($cond)) {
            return $this->then->evaluate($env);
        } else if (!empty($this->else)) {
            return $this->else->evaluate($env);
        } else {
            return new Value(null);
        }
    }
    protected function isTruthy(IRenderable $val) {
        if ($val instanceof Value) {
            return (bool) $val->value;
        } else {
            // TODO: notion of empty ElemList and AttribList
            return true;
        }
    }
}

class Element implements IRenderable {
    public $type;
    public $children;
    function __construct($type, ElementList $children) {
        $this->type = $type;
        $this->children = $children;
    }
    function evaluate(array $env=[]) {
        return new self($this->type, $this->children->evaluate($env));
    }
    function render(IRenderer $renderer) {
        return $renderer->renderElement($this->type, $this->children);
    }
    function __invoke(array $env) {
        $let = $this;
        foreach ($env as $key => $val) {
            $let = new Let($key, Value::unit($val), $let);
        }
        return $let;
    }
}

class ElementList implements IRenderable, \IteratorAggregate {
    public $elements = [];
    static function fromArray(array $elems) {
        $list = new self;
        foreach ($elems as $el) {
            if (self::isAssoc($el)) {
                $el = AttributeList::fromArray($el);
            } else {
                $el = Value::unit($el);
            }
            $list->add($el);
        }
        return $list;
    }
    static protected function isAssoc($it) {
        if (!is_array($it)) {
            return false;
        }
        foreach (array_keys($it) as $k => $v) {
            if ($k !== $v) {
                return true;
            }
        }
        return false;
    }
    /** INVARIANT: Attributes must always come before any other IView */
    function add(IView $element) {
        // TODO: change into a priority queue
        if ($element instanceof Attribute) {
            array_unshift($this->elements, $element);
        } else {
            $this->elements[] = $element;
        }
    }
    function getIterator() {
        return new \ArrayIterator($this->elements);
    }
    function evaluate(array $env=[]) {
        $newList = new self;
        foreach ($this->elements as $el) {
            $newList->add($el->evaluate($env));
        }
        return $newList;
    }
    function render(IRenderer $renderer) {
        return $renderer->renderElementList($this->elements);
    }
}

class Attribute implements IRenderable {
    public $name;
    public $value;
    function __construct(IView $name, IView $value) {
        $this->name = $name;
        $this->value = $value;
    }
    function evaluate(array $env=[]) {
        $name = $this->name->evaluate($env);
        $value = $this->value->evaluate($env);
        if ($value instanceof Attribute) {
            return $value;
        } else if (!$name instanceof Value) {
            throw new \InvalidArgumentException(
                "Attribute name must evaluate to a host value.");
        } else if (!$value instanceof Value) {
            throw new \InvalidArgumentException(
                "Attribute value must evaluate into a host value or Attribute.");
        } else {
            return new self($name, $value);
        }
    }
    function render(IRenderer $renderer) {
        if ($this->value instanceof Attribute) {
            return $this->value->render($renderer);
        } else {
            return $renderer->renderAttribute($this->name, $this->value);
        }
    }
}

class AttributeList extends Attribute implements \IteratorAggregate {
    public $list = [];
    function __construct() {}
    static function fromArray(array $attrs) {
        $list = new AttributeList;
        foreach ($attrs as $name => $attr) {
            $list->add($name, Value::unit($attr));
        }
        return $list;
    }
    function add($name, IView $value) {
        if ($value instanceof Attribute) {
            $this->list[] = $value;
        } else {
            $this->list[] = new Attribute(Value::unit($name), $value);
        }
    }
    function getIterator() {
        return new ArrayIterator($this->list);
    }
    function evaluate(array $env=[]) {
        $list = [];
        foreach ($this->list as $attr) {
            $list[] = $attr->evaluate($env);
        }
        $newList = new AttributeList;
        $newList->list = $list;
        return $newList;
    }
    function render(IRenderer $renderer) {
        return $renderer->renderAttributeList($this->list);
    }
}

class Decorator implements IView {
    protected $view;
    function __construct(IView $view) {
        $this->view = $view;
    }
    function evaluate(array $env=[]) {
        return $this->view->evaluate($env);
    }
    function __invoke(array $env=[]) {
        $let = $this->view;
        foreach ($env as $key => $val) {
            $let = new Let($key, Value::unit($val), $let);
        }
        return $let;
    }
    function __get($name) {
    }
}

function element($type, $var_args=null) {
    $args = func_get_args();
    array_shift($args);
    return new Element($type, ElementList::fromArray($args));
}

function children($var_args) {
    $args = func_get_args();
    return ElementList::fromArray($args);
}

function attr($name, $val) {
    return new Attribute(Value::unit($name), Value::unit($val));
}

function attribs(array $attrs) {
    return AttributeList::fromArray($attrs);
}

function map(Name $var, callable $fn) {
    return new Apply(function ($values) use ($fn) {
        $out = new ElementList;
        foreach ($values as $val) {
            $out->add($fn($val));
        }
        return $out;
    }, [$var]);
}

function lf() { echo "\n"; }

$r = new HtmlRenderer;
$div = element('div', 'foo', new Name('foo'));
echo $r->render($div, ['foo' => 'bar']);

lf();

$li = element('li', new Name('item'), new Apply('attribs', [new Name('a')]));
$ul = element('ul', map(new Name('data'), $li), attr(new Name('foo'), 'bar'));
$env = ['data' => [
    ['item' => '<script>'], 
    ['item' => 'injection!'],
    ['item' => '</script>'],
], 'a' => ['z' => 'y', 'x' => 'b'], 'foo' => 'id'];
echo $r->render($ul, $env);

