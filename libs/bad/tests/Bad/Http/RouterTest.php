<?php


require_once dirname(__FILE__) . '/../../bootstrap.php';

/* 
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Unit and functional tests for Router.
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @license    MIT License
 */
class Bad_Http_RouterTest extends PHPUnit_Framework_TestCase
{
    protected function request($url, $method='GET', $page='index.php')
    {
        $req = new Bad_Http_Request();
        $url = '/' . trim($url, '/');
        $req->setServer(array(
            'REQUEST_METHOD' => $method,
            'REQUEST_URI' => "/{$page}{$url}",
            'SCRIPT_NAME' => $page,
            'SCRIPT_FILENAME' => "/var/www/{$page}",
            'DOCUMENT_ROOT' => '/var/www',
        ));
        return $req;
    }

    function testRequestMethodMatch()
    {
        $router = new Bad_Http_Router($this->request('/', 'POST'));
        $this->assertFalse($router->match('index.php', 'get'));
        $this->assertTrue($router->match('index.php', 'post'));
        $this->assertFalse($router->match('index.php', 'put'));
        $this->assertFalse($router->match('index.php', 'delete'));
    }

    function testMatchLiteralSegment()
    {
        $router = new Bad_Http_Router($this->request('/foo'));
        $this->assertTrue($router->match('index.php/foo'));
        $this->assertFalse($router->match('index.php'));
        $this->assertFalse($router->match('index.php/bar'));
    }

    function testMatchWithBind()
    {
        $router = new Bad_Http_Router($this->request('/abc', 'GET', ''));
        $this->assertFalse($router->match('foo'));
        $this->assertTrue($router->match(':foo'));
        $this->assertEquals('abc', $router->args()->get('foo'));
    }

    function testHelperMethodsAutomaticallyAddPageBinding()
    {
        $router = new Bad_Http_Router($this->request('/abc', 'GET', 'foo.php'));
        $args = $router->args();
        $this->assertTrue($router->GET('/abc'));
        $this->assertEquals('foo.php', $args->get('page'));
        $this->assertTrue($router->GET(':var'));
        $this->assertEquals('foo.php', $args->get('page'));
        $this->assertEquals('abc', $args->get('var'));
    }

    function testGetShortcut()
    {
        $router = new Bad_Http_Router($this->request('/abc'));
        $args = $router->args();
        $this->assertFalse($router->POST('/:foo'));
        $this->assertFalse($router->PUT('/:foo'));
        $this->assertFalse($router->DELETE('/:foo'));
        $this->assertTrue($router->GET('/:foo'));
        $this->assertEquals('abc', $args->foo);
    }

    function testPostShortcut()
    {
        $router = new Bad_Http_Router($this->request('/abc', 'POST'));
        $args = $router->args();
        $this->assertFalse($router->GET('/:foo'));
        $this->assertFalse($router->PUT('/:foo'));
        $this->assertFalse($router->DELETE('/:foo'));
        $this->assertTrue($router->POST('/:foo'));
        $this->assertEquals('abc', $args->foo);
    }

    function testPutShortcut()
    {
        $router = new Bad_Http_Router($this->request('/abc', 'PUT'));
        $args = $router->args();
        $this->assertFalse($router->GET('/:foo'));
        $this->assertFalse($router->POST('/:foo'));
        $this->assertFalse($router->DELETE('/:foo'));
        $this->assertTrue($router->PUT('/:foo'));
        $this->assertEquals('abc', $args->foo);
    }

    function testDeleteShortcut()
    {
        $router = new Bad_Http_Router($this->request('/abc', 'DELETE'));
        $args = $router->args();
        $this->assertFalse($router->GET('/:foo'));
        $this->assertFalse($router->POST('/:foo'));
        $this->assertFalse($router->PUT('/:foo'));
        $this->assertTrue($router->DELETE('/:foo'));
        $this->assertEquals('abc', $args->foo);
    }

    function testOptionalSegment()
    {
        $router = new Bad_Http_Router($this->request('/abc'));
        $args = $router->args();
        $this->assertTrue($router->GET('[abc]'));
        $this->assertTrue($router->GET('[:foo]'));
        $this->assertEquals('abc', $args->foo);

        $router = new Bad_Http_Router($this->request('/'));
        $args = $router->args();
        $this->assertTrue($router->GET('[abc]'));
        $this->assertTrue($router->GET('[:foo]'));
        $this->assertTrue($router->GET('[:bar = whatever]'));
        $this->assertEquals('whatever', $args->bar);
    }

    function testTwoOptionalSegmentsOneWithBind()
    {
        $patt = '/foo/[bar]/[:baz=Lorem Ipsum]';

        $router = new Bad_Http_Router($this->request('/foo/any'));
        $this->assertTrue($router->GET($patt));
        $this->assertEquals('any', $router->args()->baz);

        $router = new Bad_Http_Router($this->request('/foo/bar/any'));
        $this->assertTrue($router->GET($patt));
        $this->AssertEquals('any', $router->args()->baz);

        $router = new Bad_Http_Router($this->request('/foo/bar'));
        $this->assertTrue($router->GET($patt));
        $this->assertEquals('Lorem Ipsum', $router->Args()->baz);

        $router = new Bad_Http_Router($this->request('/foo'));
        $this->assertTrue($router->GET($patt));
        $this->AssertEquals('Lorem Ipsum', $router->args()->baz);
    }

    function testConstrainedSegment()
    {
        $router = new Bad_Http_Router($this->request('/view/12345'));
        $this->assertTrue($router->GET('/view/:id(\d+)'));

        $router = new Bad_Http_Router($this->request('/view/invalid id'));
        $this->assertFalse($router->GET('/view/:id(\d+)'));
    }

    function testConstrainedOptionalSegmentWithDefaultWillMatchIfSegmentIsAbsent()
    {
        $router = new Bad_Http_Router($this->request('/view'));
        $this->assertTrue($router->GET('/view/[:id(\d+) = 1]'));
        $this->assertEquals('1', $router->Args()->id);
    }

    function testConstrainedSegmentWithDefaultWontMatchIfSegmentIsPresentAndFailsTheTest()
    {
        // This fails only because there's no pattern to catch "invalid-id" when
        // it fails to match the :id constraint. When I implement a splat
        // matcher, this would be solved elegantly. For now, put an
        // optional bind at the end of the pattern.
        $router = new Bad_Http_Router($this->request('/view/invalid-id'));
        $this->assertFalse($router->GET('/view/[:id(\d+) = 1]'));
        $this->assertTrue($router->GET('/view/[:id(\d+)=1]/[:bad]'));
        $this->assertEquals('1', $router->args()->id);
        $this->assertEquals('invalid-id', $router->args()->bad);
    }

    function testRootPatternShouldNotMatchLongerUrls()
    {
        $router = new Bad_Http_Router($this->request('/foo/bar/baz/bat'));
        $this->assertFalse($router->GET('/'), 'root pattern matcher is too lax');
    }
}
