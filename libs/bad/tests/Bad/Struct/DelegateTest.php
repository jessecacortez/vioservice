<?php

require_once dirname(__FILE__) . '/../../bootstrap.php';

/* 
 * This file is a part of the bad project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Unit and functional tests for Delegate.
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad Ideas
 * @license    MIT License
 */
class Bad_Struct_DelegateTest extends PHPUnit_Framework_TestCase
{
    /**
     *
     * @var Bad_Struct_Delegate 
     */
    public $sut;

    public function setup()
    {
        $this->sut = new Bad_Struct_Delegate();
    }

    function testCanBeAccessedLikeAnArray()
    {
        $this->sut['foo'] = 'bar';
        $this->assertEquals('bar', $this->sut['foo']);
        unset($this->sut['foo']);
        $this->assertTrue(empty($this->sut['foo']));
        $this->assertFalse(isset($this->sut['foo']));
    }

    function testLooksUpValuesFromPrototypeIfNotSet()
    {
        $this->assertFalse(isset($this->sut['foo']));
        $this->sut->setPrototype(new Bad_Struct_Delegate(array('foo' => 'bar')));
        $this->assertTrue(isset($this->sut['foo']));
        $this->assertEquals('bar', $this->sut['foo']);
    }

    function testPrototypeValuesAreIncludedInArrayCopy()
    {
        $p = new Bad_Struct_Delegate(array('foo' => 'bar'));
        $this->sut->setPrototype($p);
        $this->sut['bar'] = 'baz';
        $this->assertEquals(array('foo' => 'bar', 'bar' => 'baz'),
                $this->sut->getArrayCopy());

        $p->setPrototype(new Bad_Struct_Delegate(array('baz' => 'quux')));
        $this->assertEquals(array('foo' => 'bar', 'bar' => 'baz', 'baz' => 'quux'),
                $this->sut->getArrayCopy(), "array copy 2-level prototype");
    }

    function testExcludesPrototypeValuesWhenKeyExists()
    {
        $this->sut->setPrototype(new Bad_Struct_Delegate(array('foo' => 'bar')));
        $this->sut['foo'] = 'not bar';
        $this->assertTrue(isset($this->sut['foo']));
        $this->AssertEquals(array('foo' => 'not bar'), $this->sut->getArrayCopy());
    }

    function testIssetReturnsTrueWhenAbsentInObjButPresentInPrototype()
    {
        $this->assertFalse(isset($this->sut['foo']));
        $this->sut->setPrototype(new Bad_Struct_Delegate(array('foo' => 'bar')));
        $this->assertTrue(isset($this->sut['foo']));
    }

    function testIssetReturnsFalseWhenUnsetEvenIfItExistsInPrototype()
    {
        $this->sut->setPrototype(new Bad_Struct_Delegate(array('foo' => 'bar')));
        $this->sut['foo'] = 'bar';
        unset($this->sut['foo']);
        $this->assertFalse(isset($this->sut['foo']));
    }

    function testSetDoesNotTouchPrototype()
    {
        $this->sut['bar'] = 'baz';
        $a = $this->sut->extend();
        $a['foo'] = 'bar';
        $a['bar'] = 'not baz';
        $this->assertTrue(isset($a['foo']));
        $this->assertFalse(isset($this->sut['foo']));
        $this->assertNotEquals('not baz', $this->sut['bar']);
    }

    function testUnsetDoesNotTouchPrototype()
    {
        $this->sut['foo'] = 'bar';
        $a = $this->sut->extend();
        $a['foo'] = 'not bar';
        unset($a['foo']);
        $this->assertTrue(isset($this->sut['foo']));
    }

    function testItsOkayToUnsetNonExistentKeys()
    {
        $this->assertFalse(isset($this->sut['foo']));
        unset($this->sut['foo']);
    }
}
