<?php

require_once dirname(__FILE__) . '/../../bootstrap.php';

/* 
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Unit and functional tests for PathMap.
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @license    MIT License
 */
class Bad_Struct_PathMapTest extends PHPUnit_Framework_TestCase
{
    public $sut;

    public function setup()
    {
        $this->sut = new Bad_Struct_PathMap();
    }

    function testIsLoadable()
    {
        $this->assertTrue($this->sut instanceof Bad_Struct_PathMap);
    }

    function testSetAndGetValue()
    {
        $test = $this->sut;
        $test->set('foo', 'FOO');
        $this->AssertEquals('FOO', $test->get('foo'));
    }

    function testSetAndGetValueDeep()
    {
        $test = $this->sut;
        $test->set(array('foo', 'bar', 'baz'), 123);
        $this->assertEquals(123, $test->get(array('foo', 'bar', 'baz')));
        $this->assertEquals(array('baz' => 123), $test->get(array('foo', 'bar')));
        $this->assertEquals(array('bar' => array('baz' => 123)), $test->get('foo'));
    }

    function testMostRecentlyAddedDefaultWins()
    {
        $test = $this->sut;
        $test->addDefaults(array('foo' => 1))
             ->addDefaults(array('foo' => 2));
        $this->AssertEquals(2, $test->get('foo'));
    }

    function testOwnValueWinsOverDefaults()
    {
        $test = new Bad_Struct_PathMap(array('foo' => 0));
        $test->addDefaults(array('foo' => 1))
             ->addDefaults(array('foo' => 2));
        $this->assertEquals(0, $test->get('foo'));
    }

    function testFlattenValuesMostRecentDefaultWins()
    {
        $test = new Bad_Struct_PathMap(array('foo' => 0));
        $test->addDefaults(array('bar' => 1, 'baz' => 2))
             ->addDefaults(array('bar' => 0));
        $this->assertEquals(array('foo' => 0, 'bar' => 0, 'baz' => 2),
            $test->flatten());
    }

    function testFlattenValuesMergesArrays()
    {
        $this->markTestSkipped("do i really need this?");
        $test = new Bad_Struct_PathMap(array('foo' => array('foo' => 0)));
        $test->addDefaults(array('foo' => array('bar' => 1)));
        $test->addDefaults(array('foo' => array('baz' => 2)));
        $this->AssertEquals(array('foo' => array('foo' => 0, 'bar' => 1, 'baz' => 2)),
            $test->flatten());
    }

    function testFlattenValuesMultilevelMerge()
    {
        $this->markTestSkipped("do i really need this?");
        $test = new Bad_Struct_PathMap(array('foo' => array('bar' => array('foo' => 0))));
        $test->addDefaults(array('foo' => array('bar' => array('bar' => 1))));
        $test->addDefaults(array('foo' => array('bar' => array('baz' => 2))));
        $this->AssertEquals(array('foo' => array('bar' =>  array('foo' => 0, 'bar' => 1, 'baz' => 2))),
            $test->flatten());
    }

    function testArrayAliasing()
    {
        $arr = array();
        $map = Bad_Struct_PathMap::alias($arr);
        $map->set('foo', 'bar');
        $this->assertTrue(array_key_exists('foo', $arr));
        $this->assertEquals('bar', $arr['foo']);
    }
}
