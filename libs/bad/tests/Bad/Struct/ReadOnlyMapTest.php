<?php

require_once dirname(__FILE__) . '/../../bootstrap.php';

/* 
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Unit and functional tests for ReadOnlyMap.
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @license    MIT License
 */
class Bad_Struct_ReadOnlyMapTest extends PHPUnit_Framework_TestCase
{
    function testIsLoadable()
    {
        $this->assertTrue(new Bad_Struct_ReadOnlyMap instanceof Bad_Struct_ReadOnlyMap);
    }

    function testCanBindArrays()
    {
        $a = array();
        $map = Bad_Struct_ReadOnlyMap::alias($a);
        $a['foo'] = 'bar';
        $this->assertTrue($map->tryGet('foo', $actual));
        $this->assertEquals('bar', $actual);
    }

    function testCannotChangeValues()
    {
        $a = array();
        $map = Bad_Struct_ReadOnlyMap::alias($a);
        $map->set('foo', 'bar');
        $this->assertFalse(array_key_exists('foo', $a));
    }
}
