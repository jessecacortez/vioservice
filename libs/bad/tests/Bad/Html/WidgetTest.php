<?php

require_once dirname(__FILE__) . '/../../bootstrap.php';

/* 
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Unit and functional tests for Widget.
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @license    MIT License
 */
class Bad_Html_WidgetTest extends PHPUnit_Framework_TestCase
{
    function testSimple()
    {
        $w = new Bad_Html_Widget();
        $div = $w->div();
        $this->assertEquals("<div>\n</div>", $div->render());
    }

    function testWithAttrib()
    {
        $w = new Bad_Html_Widget();
        $div = $w->div(array('id' => 'foo'));
        $this->assertEquals("<div id=\"foo\">\n</div>", $div->render());
    }

    function testWithSymbolAsChildText()
    {
        $w = new Bad_Html_Widget();
        $div = $w->div($w->text);
        $this->assertEquals("<div>\n  FOO\n</div>", $div->render(array('text' => 'FOO')));
    }

    function testUndefinedSymbolEvaluatesToBlankLine()
    {
        $w = new Bad_Html_Widget();
        $div = $w->div($w->text);
        $this->assertEquals("<div>\n  \n</div>", $div->render());
    }

    function testSymbolAsAttrib()
    {
        $w = new Bad_Html_Widget();
        $div = $w->div(array('id' => $w->id));
        $this->assertEquals("<div id=\"foo\">\n</div>", $div->render(array('id' => 'foo')));
    }

    function testSymbolAsAttribAndChildText()
    {
        $w = new Bad_Html_Widget();
        $div = $w->div(array('id' => $w->a), $w->b);
        $this->assertEquals("<div id=\"AAA\">\n  BBB\n</div>", $div->render(array('a' => 'AAA', 'b' => 'BBB')));
    }

    function testTagNamesAreEscaped()
    {
        $w = new Bad_Html_Widget();
        $bad = '<script>alert("INJECTION!")</script>';
        $foo = $w->tag($bad);
        $this->assertNotContains($bad, $foo->render());
    }

    function testValuesAreHtmlEscaped()
    {
        $w = new Bad_Html_Widget();
        $badText = '<script>alert("INJECTION!")</script>';
        $badAttrib = '"></div>' . $badText . '<div "';
        $div = $w->div(array('id' => $badAttrib));
        $out = $div->render();
        $this->assertNotContains($badText, $out);
    }

    function testResolvedSymbolsAreHtmlEscaped()
    {
        $w = new Bad_Html_Widget();
        $badText = '<script>alert("INJECTION!")</script>';
        $badAttrib = '"></div>' . $badText . '<div "';
        $div = $w->div(array('id' => $w->bad));
        $out = $div->render(array('bad' => $badAttrib));
        $this->assertNotContains($badText, $out);
    }

    function testNestedAttribsAreFlattened()
    {
        $w = new Bad_Html_Widget();
        $div = $w->div(array('id' => 'foo', 'class' => 'bar',
           $w->attribs(array(
                    'foo' => 'baz', 'bat' => 'quux'
                ))));
        $this->assertEquals(
                "<div id=\"foo\" class=\"bar\" foo=\"baz\" bat=\"quux\">\n</div>",
                $div->render());
    }

    function testResolvedNestedAttribsAreFlattened()
    {
        $w = new Bad_Html_Widget();
        $div = $w->div(array('id' => 'foo', 'class' => 'bar', $w->moreAttribs));
        $this->assertEquals(
                "<div id=\"foo\" class=\"bar\" foo=\"baz\" bat=\"quux\">\n</div>",
                $div->render(array('moreAttribs' => $w->attribs(array(
                    'foo' => 'baz', 'bat' => 'quux'
                )))));
    }

    function testDeeplyNestedAttribsAreFlattened()
    {
        $w = new Bad_Html_Widget;
        $a = $w->a(array(
            'level' => 'one',
            $w->attribs(array(
                'level' => 'two',
                $w->attribs(array(
                    'level' => 'three',
                    $w->attribs(array('id' => 'foo'))))))));
        $this->AssertEquals(
                '<a level="one" level="two" level="three" id="foo">' . "\n</a>",
                $a->render());
    }

    function testWidgetComposition()
    {
        $w = new Bad_Html_Widget();
        $span = $w->span();
        $div = $w->div($span);
        $this->assertEquals("<div>\n  <span>\n  </span>\n</div>", $div->render());
    }

    function testDeeplyNestedTags()
    {
        $w = new Bad_Html_Widget;
        $a = $w->a($w->b($w->c(), $w->d()), $w->e($w->f($w->a, $w->b)));
        $this->assertEquals(
'<a>
  <b>
    <c>
    </c>
    <d>
    </d>
  </b>
  <e>
    <f>
      foo
      bar
    </f>
  </e>
</a>'
                , $a->render(array('a' => 'foo', 'b' => 'bar')));
    }

    function testNestedChildrenAreFlattened()
    {
        $w = new Bad_Html_Widget;
        $a = $w->a($w->children(array(
            $w->b(), $w->children(array(
                'foo', $w->children(array('bar', 'baz')))), $w->c('bat'), 'quux')));
        $this->assertEquals(
'<a>
  <b>
  </b>
  foo
  bar
  baz
  <c>
    bat
  </c>
  quux
</a>'
                , $a->render());
    }

    function testLetBindings()
    {
        $w = new Bad_Html_Widget;
        $a = $w->div($w->contents);
        $div = $w->let('contents', 'FOO', $a);
        $this->AssertEquals("<div>\n  FOO\n</div>", $div->render());
    }

    function testLetOverridesPassedEnv()
    {
        $w = new Bad_Html_Widget;
        $a = $w->div($w->contents);
        $div = $w->let('contents', 'FOO', $a);
        $this->AssertEquals("<div>\n  FOO\n</div>", $div->render(array('contents' => 'BAR')));
    }

    function testMapWithRegularCallback()
    {
        $w = new Bad_Html_Widget;
        $ul = $w->ul($w->map($w->items, array($this, 'li')));
        $this->assertEquals(
'<ul>
  <li>
    a
  </li>
  <li>
    b
  </li>
  <li>
    c
  </li>
</ul>'
                , $ul->render(array('items' => array('a', 'b', 'c'))));
    }

    function li($x)
    {
        $c = new Bad_Html_ElemList;
        $c->add(new Bad_Html_Value($x));
        return new Bad_Html_Element('li', null, $c);
    }

    function testElementAsMapFunction()
    {
        $w = new Bad_Html_Widget;
        $items = array(
            array('value' => 1, 'desc' => 'one'),
            array('value' => 2, 'desc' => 'two'),
            array('value' => 3, 'desc' => 'three'),
        );
        $opt = $w->option(array('value' => $w->value), $w->desc);
        $sel = $w->select($w->map($w->items, $opt));
        $this->assertEquals(
'<select>
  <option value="1">
    one
  </option>
  <option value="2">
    two
  </option>
  <option value="3">
    three
  </option>
</select>'
                , $sel->render(array('items' => $items)));
    }

    function testSpecialShortcutForElementMap()
    {
        $w = new Bad_Html_Widget;
        $li = $w->li($w->_);
        $items = array('first', 'second', 'third');
        $ul = $w->ul($w->map($w->items, $li));
        $this->assertEquals(
'<ul>
  <li>
    first
  </li>
  <li>
    second
  </li>
  <li>
    third
  </li>
</ul>'
                , $ul->render(array('items' => $items)));
    }

    function testNestedMap()
    {
        $w = new Bad_Html_Widget;
        $table = $w->table($w->map($w->data, 
            $w->tr($w->map($w->_,
                $w->td($w->_)))));
        $this->AssertEquals( 
'<table>
  <tr>
    <td>
      1
    </td>
    <td>
      2
    </td>
    <td>
      3
    </td>
  </tr>
  <tr>
    <td>
      4
    </td>
    <td>
      5
    </td>
    <td>
      6
    </td>
  </tr>
</table>'
                , $table->render(array('data' => array(
                    array(1,2,3), array(4,5,6),
                ))));
    }

    function testSimpleConditional()
    {
        $w = new Bad_Html_Widget;
        $errors = $w->when($w->errors, 
                    $w->ul(array('class' => 'errors'), 
                        $w->map($w->errors, $w->li($w->_))));
        $elem = $w->li(
                    $w->input(array('name' => $w->name, 'value' => $w->value)),
                    $errors);
        $form = $w->form(array($w->attr),
                    $w->ol(array('class' => 'list-form'), 
                        $w->map($w->elements, $elem)));
        $this->assertEquals( 
'<form method="POST" action="/submit">
  <ol class="list-form">
    <li>
      <input name="Username" value="FooBar" />
    </li>
    <li>
      <input name="Password" value="hunter2" />
      <ul class="errors">
        <li>
          too short
        </li>
        <li>
          too common
        </li>
      </ul>
    </li>
  </ol>
</form>'
                , $form->render(array(
            'attr' => $w->attribs(array(
                'method' => 'POST',
                'action' => '/submit',
            )),
            'elements' => array(
                array('name' => 'Username', 'value' => 'FooBar'),
                array('name' => 'Password', 'value' => 'hunter2', 'errors' => array(
                    'too short', "too common",
                ))
            )
        )));
    }

    function testShortCircuitAndEvalsToTheRightExprIfLeftIsTruthy()
    {
        $w = new Bad_Html_Widget;
        $a = $w->a($w->both($w->huh, $w->br()));
        $exp = "<a>\n  <br />\n</a>";
        $this->assertEquals($exp, $a->render(array('huh' => 1)));
        $this->assertEquals($exp, $a->render(array('huh' => true)));
        $this->assertEquals($exp, $a->render(array('huh' => '1')));
        $this->assertNotEquals($exp, $a->render());
        $this->assertNotEquals($exp, $a->render(array('huh' => 0)));
        $this->assertNotEquals($exp, $a->render(array('huh' => false)));
        $this->assertNotEquals($exp, $a->render(array('huh' => '')));
        $this->assertNotEquals($exp, $a->render(array('huh' => '0'))); // wtf?
        // empty array is also falsy
    }

    function testShortCircuitOrEvalsToTheRightExprIfLeftIsFalsy()
    {
        $w = new Bad_Html_Widget;
        $a = $w->a($w->either($w->huh, $w->br()));
        $exp = "<a>\n  <br />\n</a>";
        $this->assertEquals($exp, $a->render());
        $this->assertEquals($exp, $a->render(array('huh' => 0)));
        $this->assertEquals($exp, $a->render(array('huh' => false)));
        $this->assertEquals($exp, $a->render(array('huh' => '')));
        $this->assertEquals($exp, $a->render(array('huh' => '0'))); // wtf?
        $this->assertnotEquals($exp, $a->render(array('huh' => 1)));
        $this->assertnotEquals($exp, $a->render(array('huh' => true)));
        $this->assertnotEquals($exp, $a->render(array('huh' => '1')));
    }

    function ApiBrainstorming()
    {
        $w = new Bad_Html_Widget();





    }
}
