<?php

require_once dirname(__FILE__) . '/../bootstrap.php';

/* 
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Unit and functional tests for Application.
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @license    MIT License
 */
class Bad_ApplicationTest extends PHPUnit_Framework_TestCase
{
    function testProvideServiceSpecs()
    {
        $services = array('foo' => 'stdclass');
        $app = new Bad_Application($services);
        $this->AssertTrue($app->services()->get('foo') instanceof stdclass);
    }

    function testRetrieveServiceViaMagicCall()
    {
        $app = new Bad_Application(array('foo' => 'stdclass'));
        $this->AssertSame($app->foo, $app->services()->get('foo'));
    }

    function testProvideInstantiatedService()
    {
        $foo = new stdclass;
        $app = new Bad_Application(array('foo' => $foo));
        $this->assertSame($foo, $app->foo);
    }

    function testSharesServicesWithOtherServiceLocators()
    {
        $bar = new stdclass;
        $services = array(
            'foo' => 'stdclass',
            'bar' => $bar,
            'subService' => 'Bad_Di_Locator',
        );
        $app = new Bad_Application($services);
        $this->assertTrue($app->subService->has('foo'));
        $this->assertSame($app->foo, $app->subService->get('foo'));
        $this->assertSame($bar, $app->subService->get('bar'));
    }
}
