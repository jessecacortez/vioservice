<?php

require_once dirname(__FILE__) . '/../../bootstrap.php';

/* 
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Unit and functional tests for StringPattern.
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @license    MIT License
 */
class Bad_Matching_StringPatternTest extends PHPUnit_Framework_TestCase
{
    function testLiteralsOnly()
    {
        $route = new Bad_Matching_StringPattern('/foo/bar/baz');
        $this->assertEquals(array(), $route->match(array('foo', 'bar', 'baz')));
    }

    function testOptionalLiteral()
    {
        $r = new Bad_Matching_StringPattern('/foo/[bar]/[baz]');
        $this->assertEquals(array(), $r->match(array('foo', 'bar', 'baz')));
        $this->assertEquals(array(), $r->match(array('foo', 'baz')));
        $this->assertEquals(array(), $r->match(array('foo', 'bar')));
        $this->assertEquals(array(), $r->match(array('foo')));
        $this->assertFalse($r->match(array('bar')));
        $this->assertFalse($r->match(array()));
    }

    function testCapturedSegment()
    {
        $r = new Bad_Matching_StringPattern('foo/:bar');
        $this->assertEquals(array('bar' => 'BAR'), $r->match(array('foo', 'BAR')));
    }

    function testCaptureWithRequirement()
    {
        $r = new Bad_Matching_StringPattern('foo/:bar(\w{3})');
        $this->assertFalse($r->match(array('foo', 'bars')));
        $this->assertEquals(array('bar' => 'mon'), $r->match(array('foo', 'mon')));
    }

    function testOptionalCapture()
    {
        $r = new Bad_Matching_StringPattern('foo/[:bar([A-Z]{3})]');
        $this->assertFalse($r->match(Array('foo', '123')));
        $this->assertFalse($r->match(Array('foo', 'ab')));
        $this->assertFalse($r->match(Array('foo', 'abcd')));
        $this->assertEquals(array(), $r->match(array('foo')));
        $this->assertEquals(array('bar' => 'ABC'), $r->match(Array('foo', 'ABC')));
    }

    function testOptionalCaptureWithDefault()
    {
        $r = new Bad_Matching_StringPattern('[:foo(\w{3})]/[:bar = BAR]');
        Bad_Debug::out($r->compile());
        $this->assertEquals(array('bar' => 'BAR'), $r->match(array()));
        $this->assertEquals(array('foo' => 'FOO', 'bar' => 'BAR'), $r->match(array('FOO')));
        $this->assertEquals(array('foo' => 'FOO', 'bar' => 'asdf'), $r->match(array('FOO', 'asdf')));
    }
}