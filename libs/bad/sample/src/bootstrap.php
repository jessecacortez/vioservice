<?php

/* 
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

$dir = dirname(__FILE__);
$projectDir = $dir . "/../..";

require_once $projectDir . "/Bad/Loader.php";

Bad_Loader::autoload();

$zendLoader = new Bad_Loader("Zend", $projectDir . "/lib/zend/Zend");
$zendLoader->register();

$appLoader = new Bad_Loader("Sample", $dir, array(
    "Handler" => $dir . "/handlers",
    "Model" => $dir . "/models",
));
$appLoader->register();

$config = require $dir . "/config.php";
return new Bad_Application($config);
