<?php

/* 
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

$app = require_once dirname(__FILE__) . "/src/bootstrap.php";

$http = $app->start();

switch (true) {

    case $http->GET("/"):
        $app->handler = new Sample_Handler_Front();
        break;

    default:
        $app->error("No handler for this route!", 404);
        break;
}

$app->run();