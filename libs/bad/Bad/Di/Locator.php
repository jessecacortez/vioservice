<?php

/*
 * This file is a part of the bad project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Service locator. Performs constructor and setter injection.
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Di_Locator extends Bad_Struct_PathMap
{
    /**
     * @var array
     */
    protected $resolving = array();

    /**
     * Locator -> ()
     * 
     * Use another locator's resources.
     *  
     * @param Bad_Di_Locator $that
     */
    function integrate(Bad_Di_Locator $that)
    {
        $this->store->pushPrototype($that->store);
    }

    /**
     * str|int, str|str[] -> any
     * 
     * Retrieves an instantiated object, or resolves one, caches and returns it.
     *
     * @param string $key Array would actually work too, so long as the value
     *                    is resolvable, but you should stick with String keys
     *                    here as much as possible.
     * @param array|string $default The spec to pass to make if the key isn't set
     * @return mixed
     */
    function get($key, $default=null)
    {
        if (!$this->tryGet($key, $SPEC)) {
            return $this->make(empty($default) ? $key : $default);
        } else {
            $seen = array();
            while (is_string($SPEC)) {
                // continue resolving while the value is a string key
                $key = $SPEC;
                if (in_array($key, $seen)) {
                    $SPEC = array();
                    break;
                }
                $seen[] = $key;
                $this->tryGet($key, $SPEC);
            }
            if (is_object($SPEC)) {
                return $SPEC;
            } else {
                return $this->make($key, $SPEC);
            }
        }
    }

    /**
     * str, str[] -> any
     * 
     * Instantiates an object from a class, injecting constructor dependencies
     * and optionally calls methods on it (with resolved objects or actual 
     * values) afterwards.
     *
     * @param string $cls The classname
     * @param array $spec May contain keys 'inject', 'invoke', and 'factory'.
     *                    'factory' should be a callable. 'inject' and 'invoke'
     *                    are lists of method names and arguments. The
     *                    difference is that 'inject' values are resolved first 
     *                    before being passed while 'invoke' values are passed 
     *                    as-is.
     * @return mixed
     */
    function make($cls, $spec=array())
    {
        if (in_array($cls, $this->resolving)) {
            throw new RuntimeException('cyclic dependency?'); 
            // TODO: make a framework exception class
        }
        $this->resolving[] = $cls;

        if (empty($spec['factory'])) {
            $rc = new ReflectionClass($cls);
            $cons = $rc->getConstructor();
        } else {
            $cons = $spec['factory'];
            if (is_array($cons)) {
                $cons = new ReflectionMethod($cons);
            } else {
                $cons = new ReflectionFunction($cons);
            }
        }

        if (empty($cons)) {
            $inst = $rc->newInstance();
        } else {
            $args = array_map(array($this, 'injectConsArgs'), 
                $cons->getParameters());
            $inst = !empty($rc) ? $rc->newInstanceArgs($args) 
                                : $cons->invokeArgs($args);
        }

        if (!empty($spec)) {
            $spec += array('inject' => array(), 'invoke' => array());
            foreach ($spec['inject'] as $args) {
                $method = array_shift($args);
                $args = array_map(array($this, 'get'), $args);
                call_user_func_array(array($inst, $method), $args);
            }
            foreach ($spec['invoke'] as $args) {
                $method = array_shift($args);
                call_user_func_array(array($inst, $method), $args);
            }
        }

        $this->store->pushDown($cls, $inst);
        array_pop($this->resolving);
        return $inst;
    }

    /**
     * ReflectionParameter -> any
     * 
     * Retrieves the typehint from the reflected parameter and returns a 
     * resolved object
     *
     * @param ReflectionParameter $rparam
     * @return mixed
     */
    protected function injectConsArgs(ReflectionParameter $rparam)
    {
        $typeHint = $rparam->getClass();
        if (empty($typeHint)) {
            if ($rparam->isDefaultValueAvailable()) {
                return $rparam->getDefaultValue();
            } else {
                // TODO: throw?
            }
        } else {
            return $this->get($typeHint->getName());
        }
    }
}

