<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Response
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Http_Response
{
    protected $body;
    protected $headers;
    protected $status;
    protected $headersSent = false;
    protected $cookies = array();
    
    /**
     * @var Bad_Struct_ReadOnlyMap[]
     */
    protected $reader = array();

    /**
     * () -> ()
     * str[] -> ()
     * str[], str -> ()
     * str[], str, str|int -> ()
     * 
     * @param array $headers
     * @param string $body
     * @param string|int $status
     */
    function __construct(array $headers=array(), $body='', $status='200 OK')
    {
        $this->headers = $headers;
        $this->body = $body;
        $this->status = $status;
    }
    /**
     * () -> Http\Headers
     * 
     * @return Bad_Http_Headers
     */
    function headers()
    {
        if (empty($this->reader['headers'])) {
            $this->reader['headers'] = Bad_Http_Headers::alias($this->headers);
        }
        return $this->reader['headers'];
    }

    /**
     * str|int, any -> Http\Response
     * 
     * @param string $key
     * @param mixed $value
     * @return Bad_Http_Response
     */
    function setHeader($key, $value)
    {
        $this->headers[$key] = $value;
        return $this;
    }

    /**
     * any[] -> Http\Response
     * 
     * @param array $headers
     * @return Bad_Http_Response
     */
    function setHeaders(array $headers)
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * () -> str[]
     * 
     * @return array
     */
    function getHeaders()
    {
        return $this->headers;
    }

    /**
     * () -> Http\Cookies
     * 
     * @return Bad_Http_Cookies
     */
    function cookies()
    {
        if (empty($this->reader['cookies'])) {
            $this->reader['cookies'] = Bad_Http_Cookies::alias($this->cookies);
        }
        return $this->reader['cookies'];
    }

    /**
     * str|int -> Http\Response
     * 
     * @param string|int $status
     * @return Bad_Http_Response
     */
    function setStatus($status)
    {
        if (is_numeric($status)) {
            $status = Bad_Http::status($status);
        }
        $this->status = $status;
        return $this;
    }

    /**
     * str -> Http\Response
     * 
     * @param string $content
     * @return Bad_Http_Response
     */
    function write($content)
    {
        $this->body .= $content;
        return $this;
    }

    /**
     * () -> Http\Response
     * 
     * @return Bad_Http_Response
     */
    function clear()
    {
        $this->body = '';
        return $this;
    }

    /**
     * () -> Http\Response
     * 
     * @return Bad_Http_Response
     */
    function sendHeaders()
    {
        header('HTTP/1.1 ' . $this->status);
        header('Status: ' . $this->status);
        foreach ($this->headers as $key => $val) {
            // TODO: escape
            header($key . ': ' . $val);
        }
        foreach ($this->cookies as $name => $cookie) {
            extract($cookie);
            $this->setCookie($name, $value, $expires, $path, $domain, 
                $secure, $httpOnly);
        }
        $this->headersSent = true;
        return $this;
    }

    /**
     * () -> Http\Response
     * 
     * @return Bad_Http_Response
     */
    function send()
    {
        if (!$this->headersSent) {
            $this->sendHeaders();
        }
        echo $this->body;
        return $this;
    }

    /**
     * () -> str
     * 
     * @return string
     */
    function getBody()
    {
        return $this->body;
    }

    /**
     * str, str -> Http\Response
     * 
     * @param string $name
     * @param string $val
     * @return Bad_Http_Response
     */
    protected function setCookie($name, $val)
    {
        $args = func_get_args();
        if (!is_array($val)) {
            call_user_func_array('setcookie', $args);
        } else {
            foreach ($val as $k => $v) {
                $args[0] = $name . '[' . $k . ']';
                $args[1] = $v;
                call_user_func_array('setcookie', $args);
            }
        }
        return $this;
    }
    
}
