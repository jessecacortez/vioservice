<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Headers
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Http_Headers extends Bad_Struct_ReadOnlyMap
{
    /**
     * any[] -> ()
     * 
     * Optionally pass initial values.
     * 
     * @param array $values
     */
    function __construct(array $values=array())
    {
        $this->store = new Bad_Struct_CaseInsensitiveDelegate($values);
    }

    /**
     * any[] -> Http\Headers
     * 
     * @param array $values
     * @return self
     */
    static function alias(array &$values)
    {
        $inst = new self;
        $inst->store->exchangeArray($values);
        return $inst;
    }
    
    /**
     * K|K[], &any -> bool : K := str|int
     * 
     * @param string|int|array $path
     * @param &mixed $OUT
     * @return bool
     */
    public function tryGet($path, &$OUT)
    {
        if (!is_array($path)) {
            return parent::tryGet($path, $OUT);
        } else {
            $store = $this->store->getShallowCopy();
            $path = array_map('strtolower', $path);
            foreach ($path as $seg) {
                $store = array_change_key_case($store);
                if (!isset($store[$seg])) {
                    return false;
                }
                $store = $store[$seg];
            }
            $OUT = $store;
            return true;
        }
    }
}
