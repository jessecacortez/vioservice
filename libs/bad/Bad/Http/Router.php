<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Router
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Http_Router
{
    /**
     * @var Bad_Http_Request
     */
    protected $request;
    
    /**
     * @var Bad_Struct_ReadOnlyMap
     */
    protected $args;
    
    protected $baseUri;
    protected $uri;
    protected $bindings = array();

    /**
     * @param Bad_Http_Request $request
     */
    function __construct(Bad_Http_Request $request)
    {
        $this->request = $request;
        $this->args = Bad_Struct_ReadOnlyMap::alias($this->bindings);
    }

    /**
     * () -> Struct\ReadOnlyMap
     * 
     * @return Bad_Struct_ReadOnlyMap
     */
    function args()
    {
        return $this->args;
    }
    
    /**
     * () -> str
     * 
     * @return string
     */
    function getUri()
    {
        return $this->uri;
    }

    /**
     * () -> str
     * 
     * Base uri is the segment between the docroot and cwd, assuming
     * no rewrite or alias/symlink happened. If the url has been rewritten 
     * or aliased, this must be set manually in the bootstrap.
     * 
     * @return string
     */
    function getBaseUri()
    {
        if (null === $this->baseUri) {
            $server = $this->request->server();
            $cur = dirname($server->get('SCRIPT_FILENAME'));
            $root = $server->get('DOCUMENT_ROOT');
            $rootLen = strlen($root);
            $this->baseUri = $root == substr($cur, 0, $rootLen)
                ? substr($cur, $rootLen) : '';
        }
        return $this->baseUri;
    }

    /**
     * str -> Http\Router
     * 
     * @param string $uri
     * @return Bad_Http_Router
     */
    function setBaseUri($uri)
    {
        $this->baseUri = $uri;
        return $this;
    }

    /**
     * () -> Http\Router
     * 
     * @return $this
     */
    function route()
    {
        $request = $this->request;
        $server = $request->server();
        $uri = $server->get('REQUEST_URI');
        if (($queryStart = strpos($uri, '?'))) {
            $uri = substr($uri, 0, $queryStart);
        }
        $base = '/' . trim($this->getBaseUri(), '/ ');
        $baseLen = strlen($base);
        if ($base == substr($uri, 0, $baseLen)) {
            $this->uri = substr($uri, $baseLen);
            $fn = $server->get('SCRIPT_NAME'); // this is blank if REQUEST_URI
                                               // has a trailing slash. shit.
            /*
            addendum to the comment above: script name will only be blank for
            uris like `/index.php/` but it would be fine for something like
            `/index.php/foo/`
            if you can't properly match `/` with `/index.php/`,
            you should either set the base uri manually or make sure that the
            directory in docroot is not a symlink to somewhere else.
            */
            if (!empty($fn) && $fn == substr($uri, 0, strlen($fn))) {
                // TODO: is it guaranteed that $fn starts with $base?
                $relFn = substr($fn, $baseLen); 
                $path = substr($uri, $baseLen + strlen($relFn));
                // $argv[0] will be a path to script relative to base
                $argv = $path ? array_merge(array(trim($relFn, '/ ')), 
                                    explode('/', trim($path, '/ ')))
                              : array(trim($relFn, '/ '));
            } else {
                // partially rewritten; parse every segment after the base uri
                $argv = explode('/', trim($this->uri, '/ '));
            }
        } else {
            $this->uri = $uri;
            // totally rewritten; parse every segment after the host
            $argv = explode('/', trim($uri, '/ '));
        }
        $request->setArgs($argv);
        return $this;
    }

    /**
     * str -> bool
     * str, str -> bool
     * 
     * @param string $pattern
     * @param string $method
     * @return boolean
     */
    function match($pattern, $method=null)
    {
        $req = $this->request;
        if (!empty($method) && !$req->isMethod($method)) {
            return false;
        }

        if (empty($this->uri)) {
            $this->route();
        }

        $segments = $req->args()->flatten();
        // TODO: cache pattern objects
        $p = new Bad_Matching_StringPattern($pattern);
        return $p->tryMatch($segments, $this->bindings);
    }

    /**
     * str -> bool
     * () -> bool
     * 
     * @param string $pattern
     * @return bool
     */
    function get($pattern='')
    {
        return $this->match(':page/' . trim($pattern, '/'), 'GET');
    }

    /**
     * str -> bool
     * () -> bool
     * 
     * @param string $pattern
     * @return bool
     */
    function post($pattern='')
    {
        return $this->match(':page/' . trim($pattern, '/'), 'POST');
    }

    /**
     * str -> bool
     * () -> bool
     * 
     * @param string $pattern
     * @return bool
     */
    function put($pattern='')
    {
        return $this->match(':page/' . trim($pattern, '/'), 'PUT');
    }

    /**
     * str -> bool
     * () -> bool
     * 
     * @param string $pattern
     * @return bool
     */
    function delete($pattern='')
    {
        return $this->match(':page/' . trim($pattern, '/'), 'DELETE');
    }

}