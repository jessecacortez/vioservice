<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of ReadOnlyMap
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Struct_ReadOnlyMap extends Bad_Struct_PathMap
{
    /**
     * any[] -> Struct\ReadOnlyMap
     * 
     * @param array $values
     * @return self
     */
    static function alias(array &$values)
    {
        $inst = new self;
        $inst->store->exchangeArray($values);
        return $inst;
    }

    /**
     * str|int, any -> Struct\ReadOnlyMap
     * 
     * No-op; setting disallowed.
     * 
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    function set($name, $value)
    {
        /* no-op */ 
        return $this;
    }
}
