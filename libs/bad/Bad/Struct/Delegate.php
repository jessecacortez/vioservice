<?php

/*
 * This file is a part of the bad project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Delegate
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Struct_Delegate implements Bad_Struct_IDelegate
{
    /**
     * @var array 
     */
    protected $data;

    /**
     * The parent object
     * 
     * @var Bad_Struct_Delegate
     */
    protected $prototype;

    /**
     * @var stdclass 
     */
    static private $sentinel;

    /**
     * Pass initial values
     * 
     * @param array $data
     */
    function __construct(array $data=array())
    {
        $this->data = $data;
    }

    /**
     * any -> bool
     * 
     * Checks if the value is identical to the sentinel object representing
     * deleted values.
     * 
     * @param mixed $val
     * @return bool
     */
    static function isRemoved($val)
    {
        return $val === self::removedSentinel();
    }

    /**
     * () -> stdclass
     * 
     * Creates once and returns a sentinel object representing removed values.
     * 
     * @return stdclass
     */
    static protected function removedSentinel()
    {
        if (empty(self::$sentinel)) {
            self::$sentinel = new stdclass;
        }
        return self::$sentinel;
    }

    /**
     * Struct\IDelegate -> ()
     * Struct\IDelegate, bool -> ()
     * 
     * Sets the parent object.
     * 
     * @param Bad_Struct_IDelegate $proto
     * @param bool $shift If false, replaces the current prototype. Otherwise,
     *                    adds $proto to the current prototype's heirarchy 
     *                    before replacing.
     */
    function setPrototype(Bad_Struct_IDelegate $proto, $shift=false)
    {
        if ($shift && !empty($this->prototype)) {
            $proto->pushPrototype($this->prototype);
        }
        $this->prototype = $proto;
    }

    /**
     * Struct\IDelegate -> ()
     * 
     * Adds a prototype to the end of the parent chain
     * 
     * @param Bad_Struct_IDelegate $proto
     */
    function pushPrototype(Bad_Struct_IDelegate $proto)
    {
        if (empty($this->prototype)) {
            $this->prototype = $proto;
        } else {
            $this->prototype->pushPrototype($proto);
        }
    }

    /**
     * str|int, any -> ()
     *  
     * Sets the value at the first prototype in the heirarchy where $key is set.
     * 
     * @param string $key
     * @param mixed $value
     */
    function pushDown($key, $value)
    {
        if (array_key_exists($key, $this->data) || empty($this->prototype)) {
            $this->offsetSet($key, $value);
        } else {
            $this->prototype->pushDown($key, $value);
        }
    }

    /**
     * any[] -> Struct\IDelegate
     * 
     * Returns a new delegate whose parent is $this.
     * 
     * @param array $values
     * @return Bad_Struct_IDelegate
     */
    function extend(array $values = array())
    {
        $ret = new self($values);
        $ret->setPrototype($this);
        return $ret;
    }

    /**
     * str -> bool
     * 
     * Checks if the index is set. Extra checks for deleted keys for this object
     * 
     * @param string $index
     * @return bool
     */
    function offsetExists($index)
    {
        if (array_key_exists($index, $this->data)) {
            return !self::isRemoved($this->data[$index]);
        } else if (!empty($this->prototype)) {
            return $this->prototype->offsetExists($index);
        } else {
            return false;
        }
    }

    /**
     * str -> any 
     * 
     * Returns the value at the specified index. Extra checks for deleted 
     * values. Throws OutOfBoundsException if the value has been deleted or does
     * not actually exist.
     * 
     * @param string $index
     * @return mixed
     * @throws OutOfBoundsException
     */
    function offsetGet($index)
    {
        if (array_key_exists($index, $this->data)) {
            $val = $this->data[$index];
            if (!self::isRemoved($val)) {
                return $val;
            }
        } else if (!empty($this->prototype)) {
            return $this->prototype->offsetGet($index);
        }
        throw new OutOfBoundsException("Invalid index: {$index}");
    }

    /**
     * str -> ()
     * 
     * Sets the value at index to the sentinel value.
     * 
     * @param string $index
     */
    function offsetUnset($index)
    {
        $this->offsetSet($index, self::removedSentinel());
    }

    /**
     * str, any -> ()
     * 
     * @param string $index
     * @param mixed $value
     */
    function offsetSet($index, $value)
    {
        $this->data[$index] = $value;
    }

    /**
     * () -> any[]
     * 
     * Removes sentinel values, flattens Delegate values, combines with parent's
     * flattened values.
     * 
     * @return array
     */
    function getArrayCopy()
    {
        $ret = array();
        foreach ($this->data as $key => $val) {
            if (self::isRemoved($val)) {
                continue;
            }
            if ($val instanceof self) {
                $ret[$key] = $val->getArrayCopy();
            } else {
                $ret[$key] = $val;
            }
        }
        if (!empty($this->prototype)) {
            return $ret + $this->prototype->getArrayCopy();
        } else {
            return $ret;
        }
    }

    /**
     * () -> any[]
     * 
     * Get a copy of own values only. Excludes removed sentinels.
     * 
     * @return array
     */
    function getShallowCopy()
    {
        $ret = array();
        foreach ($this->data as $key => $val) {
            if (self::isRemoved($val)) {
                continue;
            }
            $ret[$key] = $val;
        }
        return $ret;
    }

    /**
     * &any[] -> ()
     * 
     * Passes by reference, so any subsequent modifications are reflected in the
     * original passed array (and vice-versa). This is the old behavior of 
     * ArrayObject::exchangeArray which was changed somewhere between 5.2 and 
     * 5.5. Seems like an undocumented fix, but I already have code relying on
     * this so I've rewritten this class to revert to this behavior.
     * I also removed the parent class because I've already reimplemented all
     * the useful methods anyway.
     * 
     * @param array $newData
     */
    function exchangeArray(array &$newData)
    {
        $this->data = &$newData;
    }
}
