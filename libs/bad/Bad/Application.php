<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Application
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Application
{
    /**
     * @var Bad_Di_SharedLocator 
     */
    protected $locator;

    /**
     * @param array $services
     */
    function __construct(array $services=array())
    {
        $this->locator = new Bad_Di_SharedLocator($services);
    }

    /**
     * () -> Di\SharedLocator
     * 
     * @return Bad_Di_SharedLocator
     */
    function services()
    {
        return $this->locator;
    }

    /**
     * str -> any
     * 
     * @param string $name
     * @return mixed
     */
    function __get($name)
    {
        return $this->locator->get($name);
    }
}
