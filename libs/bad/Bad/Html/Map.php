<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Map
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Html_Map implements Bad_Html_INode
{
    protected $fn;
    protected $list;

    function __construct($fn, Bad_Html_Name $list)
    {
        if (!is_callable($fn)) {
            throw new InvalidArgumentException("First argument must be callable.");
        }
        $this->fn = $fn;
        $this->list = $list;
    }

    /** () -> (Callable, Bad_Html_Name) */
    function deconstruct()
    {
        return array($this->fn, $this->list);
    }
}
