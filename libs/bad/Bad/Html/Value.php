<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Value
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Html_Value implements Bad_Html_INode
{
    protected $value;

    function __construct($value)
    {
        $this->value = $value;
    }

    /** () -> (any) */
    function deconstruct()
    {
        return array($this->value);
    }
}
