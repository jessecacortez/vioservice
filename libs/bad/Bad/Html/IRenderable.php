<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of IRenderable
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
interface Bad_Html_IRenderable extends Bad_Html_INode
{
    
}
