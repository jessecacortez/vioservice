<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of AttribList
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Html_AttribList implements Bad_Html_IRenderable
{
    protected $attribs = array();

    /** () -> INode{} */
    function deconstruct()
    {
        return $this->attribs;
    }

    /** str, INode -> () */
    function add($name, Bad_Html_INode $value)
    {
        $this->attribs[$name] = $value;
    }

}
