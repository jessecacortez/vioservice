<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of BinaryOp
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Html_BinaryOp implements Bad_Html_INode
{
    protected $op;
    protected $left;
    protected $right;

    function __construct($op, Bad_Html_INode $left, Bad_Html_INode $right)
    {
        $this->op = $op;
        $this->left = $left;
        $this->right = $right;
    }

    /** () -> (str, INode, INode) */
    function deconstruct()
    {
        return array($this->op, $this->left, $this->right);
    }

    /**
     * str, any[] -> BinaryOp<T>
     * 
     * @param str $op
     * @param array $operands
     */
    static function sequence($op, array $operands)
    {
        if (count($operands) < 2) {
            throw new InvalidArgumentException("I need at least 2 operands.");
        }
        $left = Bad_Html_Widget::unit(array_shift($operands));
        foreach ($operands as $expr) {
            $right = Bad_Html_Widget::unit($expr);
            $left = new self($op, $left, $right);
        }
        return $left;
    }
}
