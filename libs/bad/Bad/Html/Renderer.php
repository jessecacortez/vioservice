<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Renderer
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Html_Renderer
{
    public $indentLevel = 0;
    public $indent = "  ";

    /**
     * INode, any{} -> str
     * INode -> str
     * 
     * @param Bad_Html_INode $el
     * @param array $env
     */
    function render(Bad_Html_INode $node, array $env=array())
    {
        $el = $this->evaluate($node, $env);
        if (!$el instanceof Bad_Html_IRenderable) {
            return $this->escape((string) $el);
        }

        $parts = $el->deconstruct();
        if ($el instanceof Bad_Html_Element) {
            list($tag, $attr, $children) = $parts;    
            $tag = $this->escape($tag);
            $out = "<{$tag}";
            if (!empty($attr)) {
                $out .= $this->render($attr, $env);
            }
            if ($el->isVoid()) {
                $out .= " />";
            } else {
                $out .= ">\n";
                if (!empty($children)) {
                    $this->indentLevel++;
                    $out .= $this->indent($this->render($children, $env)) . "\n";
                    $this->indentLevel--;
                }
                $out .= $this->indent("</{$tag}>");
            }
        } else if ($el instanceof Bad_Html_AttribList) {
            $out = "";
            foreach ($parts as $k => $v) {
                $v = $this->evaluate($v, $env);
                if ($v instanceof Bad_Html_AttribList) {
                    $out .= $this->render($v, $env);
                } else if ($v instanceof Bad_Html_INode) {
                    throw new InvalidArgumentException("Bad attribute value.");
                } else {
                    $out .= " " . $this->escape($k) . '="'
                         .  $this->escape((string) $v) . '"';
                }
            }
        } else if ($el instanceof Bad_Html_ElemList) {
            $out = "";
            foreach ($parts as $child) {
                $out .= $this->indent(ltrim($this->render($child, $env))) . "\n";
            }
            $out = trim($out);
        } else {
            $out = sprintf("TODO! %s", get_class($el));
        }
        return $out;
    }

    /**
     * INode, &any{} -> any
     * 
     * @param Bad_Html_INode $node
     * @param array $env
     */
    function evaluate(Bad_Html_INode $node, array &$env=array())
    {
        if ($node instanceof Bad_Html_Value) {
            list($val) = $node->deconstruct();
            return $val;
        } else if ($node instanceof Bad_Html_Name) {
            list($key) = $node->deconstruct();
            $val = $this->lookup($env, $key);
            if ($val instanceof Bad_Html_INode) {
                return $this->evaluate($val, $env);
            } else {
                return $val;
            }
        } else if ($node instanceof Bad_Html_Conditional) {
            list($if, $then, $else) = $node->deconstruct();
            if ($this->evaluate($if, $env)) {
                return $this->evaluate($then, $env);
            } else if (!empty($else)) {
                return $this->evaluate($else, $env);
            } else {
                return null;
            }
        } else if ($node instanceof Bad_Html_Map) {
            list($fn, $list) = $node->deconstruct();
            $args = $this->evaluate($list, $env);
            $isElem = $this->isElem($fn);
            if (!empty($args)) {
                $result = new Bad_Html_ElemList;
                foreach ($args as $arg) {
                    if ($isElem && !$this->isAssoc($arg)) {
                        $arg = array("_" => $arg);
                    }
                    $result->add(call_user_func($fn, $arg));
                }
                return $result;
            } else {
                return null;
            }
        } else if ($node instanceof Bad_Html_Let) {
            list($name, $val, $expr) = $node->deconstruct();
            $env = array($name => $this->evaluate($val, $env)) + $env;
            return $this->evaluate($expr, $env);
        } else if ($node instanceof Bad_Html_BinaryOp) {
            list($op, $left, $right) = $node->deconstruct();
            switch ($op) {
                case "and":
                    $left = $this->evaluate($left, $env);
                    if ($left) {
                        return $this->evaluate($right, $env);
                    } else {
                        return $left;
                    }

                case "or":
                    $left = $this->evaluate($left, $env);
                    if ($left) {
                        return $left;
                    } else {
                        return $this->evaluate($right, $env);
                    }

                default:
                    return "TODO! binop {$op}";
            }
        } else {
            return $node;
        }
    }

    /** str -> str */
    function escape($what)
    {
        return htmlspecialchars($what);
    }

    /** str -> str */
    function indent($what)
    {
        $indent = str_repeat($this->indent, $this->indentLevel);
        return "{$indent}{$what}";
    }

    /** any{}, str, any? -> any */
    protected function lookup(array $map, $key, $default=null)
    {
        if (array_key_exists($key, $map)) {
            return $map[$key];
        } else {
            return $default;
        }
    }

    /** callable -> bool */
    protected function isElem($fn)
    {
        if (is_array($fn)) {
            $fn = $fn[0];
        }
        return $fn instanceof Bad_Html_Element;
    }

    /** any -> bool */
    protected function isAssoc($it)
    {
        if (!is_array($it)) {
            return false;
        }
        $keys = array_keys($it);
        for ($i = 0, $count = count($keys); $i < $count; ++$i) {
            if ($i != $keys[$i]) {
                return true;
            }
        }
        return false;
    }
}
