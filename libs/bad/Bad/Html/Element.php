<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Element
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Html_Element implements Bad_Html_IRenderable
{
    protected $tag;
    protected $attr;
    protected $children;
    static protected $voidElems = array(
        'img', 'br', 'hr', 'meta', 'link', 'input',
    );

    function __construct($tag, Bad_Html_AttribList $attr=null, 
            Bad_Html_ElemList $children=null)
    {
        $this->tag = $tag;
        $this->attrs = $attr;
        $this->children = $children;
    }

    /** () -> (str, INode{}, INode[]) */
    function deconstruct()
    {
        return array($this->tag, $this->attrs, $this->children);
    }

    /** () -> bool */
    function isVoid()
    {
        return in_array(strtolower($this->tag), self::$voidElems);
    }

    /**
     * any{} -> str
     * () -> str
     * 
     * @param array $env
     * @return string
     */
    function render(array $env=array())
    {
        $renderer = new Bad_Html_Renderer;
        return $renderer->render($this, $env);
    }

    /**
     * any{} -> Let
     */
    function __invoke(array $env=array())
    {
        $expr = $this;
        foreach ($env as $key => $val) {
            $expr = new Bad_Html_Let($key, Bad_Html_Widget::unit($val), $expr);
        }
        return $expr;
    }
}
