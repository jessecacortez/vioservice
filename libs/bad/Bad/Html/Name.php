<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Name
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Html_Name implements Bad_Html_INode
{
    protected $name;

    function __construct($name)
    {
        $this->name = $name;
    }

    /** () -> (str) */
    function deconstruct()
    {
        return array($this->name);
    }

}
