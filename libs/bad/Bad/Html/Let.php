<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Let
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Html_Let implements Bad_Html_INode
{
    protected $name;
    protected $value;
    protected $expr;

    function __construct($name, Bad_Html_INode $value, Bad_Html_INode $expr)
    {
        $this->name = $name;
        $this->value = $value;
        $this->expr = $expr;
    }

    /** () -> (str, INode, INode) */
    function deconstruct()
    {
        return array($this->name, $this->value, $this->expr);
    }

    /**
     * any{} -> str
     * TODO: this, Element#render and ElemList#render should be in a decorator or helper
     * 
     * @param array $env
     * @return str
     */
    function render(array $env=array())
    {
        $renderer = new Bad_Html_Renderer;
        return $renderer->render($this, $env);
    }

    /**
     * any{} -> Let
     * TODO: this, Element#__invoke and ElemList#__invoke should be in a helper or decorator
     */
    function __invoke(array $env=array())
    {
        $expr = $this;
        foreach ($env as $key => $val) {
            $expr = new Bad_Html_Let($key, Bad_Html_Widget::unit($val), $expr);
        }
        return $expr;
    }
}
