<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of Widget
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Html_Widget
{
    /** any -> INode */
    static function unit($val)
    {
        if ($val instanceof Bad_Html_INode) {
            return $val;
        } else {
            return new Bad_Html_Value($val);
        }
    }

    /**
     * str, V{}, C... -> Element : V := str | INode, C := V | Element
     * str, C... -> Element
     * str -> Element
     * 
     * @param str $name
     * @param array|Bad_Html_Element $var_args
     * @return Bad_Html_Element
     */
    function tag($name, $var_args=null)
    {
        $children = array_slice(func_get_args(), 1);
        if (is_array($var_args)) {
            $attribs = $this->attribs(array_shift($children));
        } else {
            $attribs = null;
        }
        if (empty($children)) {
            $children = null;
        } else if (count($children) == 1 && 
                $children[0] instanceof Bad_Html_ElemList) {
            $children = $children[0];
        } else {
            $children = $this->children($children);
        }
        $elem = new Bad_Html_Element($name, $attribs, $children);
        return $elem;
    }

    /**
     * any{} -> AttribList
     * 
     * @param array $attribs
     * @return Bad_Html_AttribList
     */
    function attribs(array $attribs)
    {
        $attrs = new Bad_Html_AttribList;
        foreach ($attribs as $k => $v) {
            $attrs->add($k, self::unit($v));
        }
        return $attrs;
    }

    /**
     * any[] -> ElemList
     * 
     * @param array $children
     * @return Bad_Html_ElemList
     */
    function children(array $children)
    {
        $elems = new Bad_Html_ElemList;
        foreach ($children as $e) {
            $elems->add(self::unit($e));
        }
        return $elems;
    }

    /**
     * any... -> ElemList
     * 
     * @param Bad_Html_INode $var_args
     * @return Bad_Html_ElemList
     */
    function bare($var_args)
    {
        return call_user_func(array($this, 'children'), func_get_args());
    }

    /**
     * str -> Name
     * 
     * @param str $name
     * @return Bad_Html_Name
     */
    function name($name)
    {
        return new Bad_Html_Name($name);
    }

    /**
     * Name, callable -> Map
     */
    function map(Bad_Html_Name $list, $fn)
    {
        if (is_object($fn)) {
            $fn = array($fn, '__invoke');
        }
        return new Bad_Html_Map($fn, $list);
    }

    /**
     * str, any, INode -> Let
     * 
     * @param str $name
     * @param mixed $val
     * @param Bad_Html_INode $expr
     */
    function let($name, $val, Bad_Html_INode $expr)
    {
        return new Bad_Html_Let($name, self::unit($val), $expr);
    }

    /**
     * INode, any, any -> Conditional
     * 
     * @param Bad_Html_INode $cond
     * @param mixed $then
     * @param mixed $else
     */
    function when(Bad_Html_INode $cond, $then, $else=null)
    {
        if (!empty($else)) {
            $else = self::unit($else);
        }
        return new Bad_Html_Conditional($cond, self::unit($then), $else);
    }

    /**
     * any, any... -> BinaryOp<and>
     * 
     * @param mixed $expr
     * @param mixed $var_args
     * @return Bad_Html_BinaryOp
     */
    function both($expr, $var_args)
    {
        $args = func_get_args();
        return Bad_Html_BinaryOp::sequence('and', $args);
    }

    /**
     * any, any... -> BinaryOp<or>
     * 
     * @param mixed $expr
     * @param mixed $var_args
     * @return Bad_Html_BinaryOp
     */
    function either($expr, $var_args)
    {
        $args = func_get_args();
        return Bad_Html_BinaryOp::sequence('or', $args);
    }

    /** str -> Name */
    function __get($name)
    {
        return $this->name($name);
    }

    /** str, any[] -> INode */
    function __call($name, array $args)
    {
        array_unshift($args, $name);
        return call_user_func_array(array($this, 'tag'), $args);
    }
}
