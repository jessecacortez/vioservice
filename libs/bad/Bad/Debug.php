<?php

/**
 * Utilities for adhoc debugging.
 * 
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2011-12 Mon Zafra
 * @package    BadIdeas
 * @license    MIT License
 */
class Bad_Debug
{
    /**
     * Does not generate output if true.
     * 
     * @var bool 
     */
    static protected $muted = false;

    /**
     * The php server api currently used.
     * 
     * @var string 
     */
    static protected $sapi;

    /**
     * Whether or not the script is run through the cli.
     * 
     * @return bool 
     */
    static function isCli()
    {
        if (empty(self::$sapi)) {
            self::$sapi = php_sapi_name();
        }
        return 'cli' == self::$sapi;
    }

    /**
     * Global switch to turn off debugging output.
     */
    static function shutup()
    {
        self::$muted = true;
    }

    /**
     * Implodes an array. Probably should be protected.
     * 
     * @param string $glue
     * @param array $args
     * @return string 
     */
    static public function join($glue, array $args)
    {
        if (self::$muted) {
            return '';
        }
        $ret = array();
        foreach ($args as $a) {
            if (null === $a) {
                $ret[] = 'NULL';
            } else if (false === $a) {
                $ret[] = 'FALSE';
            } else if (true === $a) {
                $ret[] = 'TRUE';
            } else if (is_array($a)) {
                $ret[] = print_r($a, true);
            } else if (is_object($a)) {
                $cls = get_class($a);
                if (!method_exists($a, '__toString')) {
                    $ret[] = $cls . ' : obj#' . self::hash($a);
                } else {
                    $ret[] = $cls . ' : ' . (string) $a;
                }
            } else {
                $ret[] = (string) $a;
            }
        }
        $src = self::getCaller();
        $out = "\n$src " . implode($glue, $ret) . "\n";
        if (!self::isCli()) {
            $out = '<pre>' . $out . '</pre>';
        }
        return $out;
    }

    /**
     * Outputs the values of the arguments passed.
     * 
     * @param mixed $var_args Accepts 1 or more values
     */
    static function out($var_args = null)
    {
        $args = func_get_args();
        echo self::join(",\n", $args);
    }

    static function here()
    {
        $out = self::getCaller() . ' here.';
        if (!self::isCli()) {
            $out = '<pre>' . $out . '</pre>';
        }
        echo $out;
    }

    /**
     * Outputs string and dies.
     * 
     * @param string $info 
     */
    static function halt($info = null)
    {
        $src = self::getCaller();
        $out = "\n$src halting...\n";
        if (!self::isCli()) {
            $out = '<pre>' . $out . '</pre>';
        }
        echo $out;
        if (func_num_args() > 0) {
            call_user_func_array('var_dump', func_get_args());
        }
        die;
    }

    /**
     * Examines the call stack and returns the class/method that invoked out().
     * 
     * @return string 
     */
    static function getCaller()
    {
        $trace = debug_backtrace(false);
        $frame = reset($trace);
        while (!array_key_exists('file', $frame) 
            || __FILE__ == $frame['file']
            || !array_key_exists('line', $frame)) {
            $frame = next($trace);
        }
        $line = $frame['line'];
        $file = $frame['file'];
        $frame = next($trace);
        $src = isset($frame['class']) 
             ? $frame['class'] . '|' . $frame['function'] 
             : $file;
        return '[' . $src . ':' . $line . ']';
    }

    /**
     * Returns the truncated object hash.
     * 
     * @param object $obj
     * @return string 
     */
    static function hash($obj)
    {
        $hash = ltrim(spl_object_hash($obj), '0');
        return preg_replace_callback('/([\da-f])\1{4,}/', 
            array(__CLASS__, 'shorten'), $hash);
    }

    /**
     * Shortens the hash by removing replacing repeated digits.
     * 
     * @param array $m
     * @return string 
     */
    static protected function shorten($m)
    {
        return $m[1] . '{' . strlen($m[0]) . '}';
    }
}
