<?php

/*
 * This file is a part of the Bad Ideas project, distributed under the terms of
 * the MIT License. Please see the LICENSE file for more information.
 */

/**
 * Description of StringPattern
 *
 * @author     Mon Zafra <monzee@gmail.com>
 * @copyright  (c)2013 Mon Zafra
 * @package    Bad_Ideas
 * @subpackage 
 * @license    MIT License
 */
class Bad_Matching_StringPattern
{
    protected $pattern;
    protected $compiled;
    protected $defaults;
    protected $separator = '/';

    /**
     * Pattern syntax (rough BNF grammar, not entirely accurate):
     * 
     * Pattern  ::= Segment*
     * Segment  ::= `/` (Lit | Bind | Option)
     * Bind     ::= Key `(` In_paren `)` | Key
     * Key      ::= `:` In_brace
     * Option   ::= `[` In_brace `]` | `[` Bind Default? `]` 
     * Default  ::= `=` In_brace
     * In_paren ::= [^)]+
     * In_brace ::= [^\]=]+
     * Lit      ::= [^:(\[] [^/]*
     * 
     * Examples:
     * /
     * /foo - matches /foo only
     * /foo/[bar] - matches /foo and /foo/bar
     * /foo/[:bar] - matches /foo and /foo/any, binds "any" to the key `bar`
     * /foo/[bar]/[:baz = Lorem Ipsum] - matches /foo, /foo/bar, 
     *                                   /foo/any (binds "any" to `baz`)
     *                                   /foo/bar/any (binds "any" to `baz`)
     * /foo/:must-be-a-number(\d+) - matches /foo/1212415671, binds "1212415671"
     *                               to `must-be-a-number`
     *                             - won't match if the 2nd segment fails the
     *                               regex match
     * 
     * @param string $pattern
     * @param array $defaults
     */
    function __construct($pattern, array $defaults=array())
    {
        $this->pattern = $pattern;
        $this->defaults = $defaults;
    }

    /**
     * () -> str
     * 
     * Generates and caches a regex string pattern to be used by match()
     * 
     * @return string
     */
    function compile()
    {
        if (empty($this->compiled)) {
            $sep = $this->separator;
            $segments = explode($sep, trim($this->pattern, $sep . ' '));
            $sep = preg_quote($sep);
            $first = true;
            $pattern = '^';
            $glue = '';
            foreach ($segments as $seg) {
                $segment = $this->parse($seg);
                if ($segment->type == 'literal') {
                    $p = $glue . preg_quote($segment->value);
                } else {
                    $regex = $segment->get('pattern', "[^{$sep}]+?") . "(?={$sep}|$)";
                    $p = $glue . "(?P<{$segment->value}>{$regex})";
                    if ($segment->tryGet('default', $def)) {
                        $this->defaults[$segment->value] = $def;
                    }
                }
                if ($segment->optional) {
                    $p = "(?:{$p})?";
                }
                $pattern .= $p;
                if (!empty($first)) {
                    $glue = $sep;
                    unset($first);
                }
            }
            $this->compiled = '#' . addcslashes($pattern, '#') . '$#';
        }
        return $this->compiled;
    }

    /**
     * str[] -> bool
     * 
     * @param array $argv
     * @return bool|array
     */
    function match(array $argv)
    {
        $regex = $this->compile();
        $ret = false;
        if (preg_match($regex, implode($this->separator, $argv), $matches)) {
            $ret = $this->defaults;
            foreach ($matches as $key => $val) {
                if (!is_int($key) && !empty($val)) {
                    $ret[$key] = $val;
                }
            }
        }
        return $ret;
    }

    /**
     * str[], &str[] -> bool
     * 
     * @param array $argv
     * @param array $OUT
     * @return boolean
     */
    function tryMatch(array $argv, array &$OUT)
    {
        $ret = $this->match($argv);
        if (false !== $ret) {
            $OUT = $ret;
            return true;
        } else {
            return false;
        }
    }

    /**
     * str -> Struct\PathMap
     * 
     * @param string $seg
     * @return Bad_Struct_PathMap
     */
    protected function parse($seg)
    {
        $patterns = array(
            'capture' => '/
              ^:(?P<name>\S[^\(]+)     # colon followed by a non-space
                                       # followed by a sequence of non-lparen
              (?P<assert>\(.*?\)$)?    # optional lparen, rparen anchored at
                                       # the end, and everything in-between.
            /x',
            'optional' => '/
              ^\[                      # sqbraces anchored at both ends
                (?:                    
                  :(?P<name>\S.*?)     # capture to name. ungreedy dot is okay 
                                       # here but not above because of the 
                                       # anchored brace at the end.
                  (?P<assert>
                    \(.*?\)            # parenthesized expr (TODO: why non-greedy?)
                      (?=              # followed by...
                        \s*=\s*        # start of defval
                      |                # or...
                        \]             # the closing sqbrace
                      )
                  )?                   # assertion still optional 
                  (?:                  
                    \s*=\s*            # equal sign (optional space around it)
                    (?P<def>[^\]]*)    # until the closing bracket
                                       # TODO: allow quoted string
                  )?                   # also optional
                |                      
                  (?P<lit>.*?)         # this is an optional literal
                )
              \]$   # maybe the default value should be outside the braces?
            /x',
            // TODO: splat pattern
            // TODO: assertion without binding (wildcard)
        );

        $ret = new Bad_Struct_PathMap();

        if (preg_match($patterns['capture'], $seg, $m)) {
            $ret->type = 'captured';
            $ret->value = $m['name'];
            if (!empty($m['assert'])) {
                $pattern = substr($m['assert'], 1, -1);
                $ret->pattern = $pattern;
            }
            if (!empty($this->defaults[$m['name']])) {
                $ret->optional = true;
            }
        } else if (preg_match($patterns['optional'], $seg, $m)) {
            $ret->optional = true;
            if (!empty($m['name'])) {
                $ret->type = 'captured';
                $ret->value = $m['name'];
                if (!empty($m['assert'])) {
                    $pattern = substr($m['assert'], 1, -1);
                    $ret->pattern = $pattern;
                }
                if (!empty($m['def'])) {
                    $ret->default = $m['def'];
                }
            } else {
                $ret->type = 'literal';
                $ret->value = $m['lit'];
            }
        } else {
            $ret->type = 'literal';
            $ret->value = $seg;
        }

        return $ret;
    }

}
